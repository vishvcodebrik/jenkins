import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DreamCasinoComponent } from './dream-casino.component';

describe('DreamCasinoComponent', () => {
  let component: DreamCasinoComponent;
  let fixture: ComponentFixture<DreamCasinoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DreamCasinoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DreamCasinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
