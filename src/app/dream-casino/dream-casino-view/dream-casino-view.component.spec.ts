import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DreamCasinoViewComponent } from './dream-casino-view.component';

describe('DreamCasinoViewComponent', () => {
  let component: DreamCasinoViewComponent;
  let fixture: ComponentFixture<DreamCasinoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DreamCasinoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DreamCasinoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
