import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndianPokerViewComponent } from './indian-poker-view.component';

describe('IndianPokerViewComponent', () => {
  let component: IndianPokerViewComponent;
  let fixture: ComponentFixture<IndianPokerViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndianPokerViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndianPokerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
