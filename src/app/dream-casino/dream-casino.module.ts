import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DreamCasinoRoutingModule } from './dream-casino-routing.module';
import { DreamCasinoComponent } from './dream-casino.component';
import { DreamCasinoViewComponent } from './dream-casino-view/dream-casino-view.component';
import {ModalModule} from 'ngx-bootstrap';
import { IndianPokerViewComponent } from './indian-poker-view/indian-poker-view.component';
import { VirtualComponent } from './virtual/virtual.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  declarations: [DreamCasinoComponent, DreamCasinoViewComponent, IndianPokerViewComponent, VirtualComponent],
  imports: [
    CommonModule,
    ModalModule,
    DreamCasinoRoutingModule,
    SlickCarouselModule
  ]
})
export class DreamCasinoModule { }
