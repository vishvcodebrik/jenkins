import {Component, OnInit, ViewChild} from '@angular/core';
import { StudioService } from '../services/studio.service';
import {UtilityService} from '../globals/utilityService';
import {ActivatedRoute, Router,NavigationEnd} from '@angular/router';
import {ModalDirective} from 'ngx-bootstrap';
declare let _: any;
@Component({
  selector: 'app-dream-casino',
  templateUrl: './dream-casino.component.html',
  styleUrls: ['./dream-casino.component.scss']
})
export class DreamCasinoComponent implements OnInit {
  @ViewChild('instructionModal', { static: false }) instructionModal: ModalDirective;
  slideConfig = {"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true,  "vertical": false , arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studioService: StudioService,
    private utilityService: UtilityService
  ) { }
  gameList;
  groupList;
  groupLists;
  product = ['Evolution Gaming', 'eBET' ,'Super Spade Games', 'Pragmatic Play Live', 'Asia Gaming', 'Evoplay Entertainment', 'Betsoft', 'Ezugi'];
  gameLists;
  userId = this.utilityService.returnLocalStorageData('userId');
  ngOnInit() {
    this.getCasinoList();
    let ite = localStorage.getItem('casinoList');
    this.gameList = JSON.parse(ite);
  }
  closeModal(){
    this.instructionModal.hide();
  }
  getCasinoList(){
    let data = {
      product : this.product
    }
    this.studioService.getGameList(data).subscribe(response => {
      if(response.status == true){
        this.gameLists = response.data;
        this.gameList = response.data;
        // this.instructionModal.show();
        this.groupList = _.chain(this.gameLists)
        // Group the elements of Array based on `color` property
          .groupBy("product")
          // `key` is group's name (color), `value` is the array of objects
          .map((value, key) => ({ name: key, category: value }))
          .value();
      }else{
        this.gameList = [];
      }
    });
  }
  onSelect(item){
    if(item === 'all'){
      this.gameList = this.gameLists;
    }else {
      this.gameList = item.category;
    }
  }

  popular(data){
    this.groupLists = _.filter(data, function(p){
      let checktrue = _.includes([3884,3897,3900,3906,3920,2071,2072,2082,2093,2099,2113,2139,2140,2144,2149,2151,2152,2154, 2156,2157, 2899,3935, 3947, 8508, 9754, 628,629, 630, 631, 633, 771, 816, 833, 1677,8984,7185, 430, 434, 1832, 1839, 2643, 2648, 2652, 2659, 2662, 2663, 2667, 6506, 6507, 6508,6509,6510, 6511, 6512,6513, 6514, 6515, 7171, 7173,7175, 7177, 7179, 7181, 7183, 7187, 7189,7191, 7193, 9511,9648, 9708, 10641, 10643, 10645, 10729, 11548, 11564, 11598, 12332, 12333, 12834, 13216], p.game_id);
      if(checktrue === true){
        return p;
      }
    });
      this.gameList = this.groupLists;
  }

  gameUrl(data){
    this.router.navigateByUrl('/dc/game-lobby/' + data.game_id);
    // let game_id = data.game_id;
    // let params = {
    //   game_id : data.game_id,
    //   user : this.userId,
    //   platform : 'GPL_DESKTOP',
    //   lobby_url : "https://crown777.games/dashboard",
    // }
    // this.studioService.getUrl(params).subscribe(response => {
    //   console.log(response);
    // });
  }




}
