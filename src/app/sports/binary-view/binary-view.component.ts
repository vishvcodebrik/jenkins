import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonService} from "../../services/common.service";
import {PlacebetService} from "../../services/placebet.service";
import {MarketService} from "../../services/market.service";
import { UserService } from '../../services/user.service';
import {CurrencyService} from "../../services/currency.service";
import {NgxSpinnerService} from "ngx-spinner";
// import {LockGameService} from "../../services/lockGame.service";
import {SocketServiceClient} from "../../globals/socketServiceClient";
import {FancyService} from "../../services/fancy.service";
import {SocketService} from "../../globals/socketService";
import { SocketServiceBbMarket } from './../../globals/socketServiceBbMarket';
// import { SocketServiceDotnetStock } from './../../globals/socketServiceDotnetStock';

// import {DragulaService } from 'ng2-dragula';
import * as $ from 'jquery';
import {ModalDirective} from "ngx-bootstrap";
// import {BinaryService} from "../../services/binary.service";
import {UtilityService} from "../../globals/utilityService";
import { interval } from 'rxjs';
var aes256 = require('aes256');
import * as env from '../../globals/env';
import {formatDate} from '@angular/common';
import * as moment from 'moment';
import {BinaryService} from '../../services/binary.service';
import {SocketServiceDotnetStock} from '../../globals/socketServiceDotnetStock';

declare let _:any;
@Component({
  selector: 'app-binary-view',
  templateUrl: './binary-view.component.html',
  styleUrls: ['./binary-view.component.scss']
})
export class BinaryViewComponent implements OnInit , OnDestroy {
  @ViewChild(ModalDirective, {static: false}) modal: ModalDirective;
  @ViewChild('infoModal', {static: false}) infoModal: ModalDirective;
  @ViewChild('openBetModal', {static: false}) openBetModal: ModalDirective;
  @ViewChild('confrimModal', {static: false}) confrimModal: ModalDirective;
  @ViewChild('removeScript', {static: false}) removeScript: ModalDirective;
  @ViewChild("placebetForm", { static: false }) placebetFormReset;
  @ViewChild('focusInput' ,  {static: false}) focusInput: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private socketService : SocketService,
    private socketServiceClient : SocketServiceClient,
    private fancyService : FancyService,
    private marketService : MarketService,
    // private lockGameService : LockGameService,
    private commonService: CommonService,
    private binaryService: BinaryService,
    private currencyService: CurrencyService,
    private utilityService: UtilityService,
    private placebetService : PlacebetService,
    // private dragulaService: DragulaService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private socketServiceBbMarket : SocketServiceBbMarket,
    private socketServiceDotnetStock : SocketServiceDotnetStock,
    private userService: UserService
  ) {
    this.socketServiceBbMarket.connect();
  }

  moduleList = [];
  moduleList1 = [];
  addDefaultData = [];

  searchVal:any;
  totalPL = 0;
  allDeals:any;
  marketStatus : any;
  informationData:any;
  completedOrder:any;
  pendingOrder:any;
  getAllExchng:any;
  filterData = false;
  selectedValue= 'ALL';
  filterDataObjects:any;
  transactionResponse:any;
  userLoginId:any;
  openPositionResponse:any;
  allBalance:any;
  instrumentTokenIds = [];
  defaultScript: any = [];
  randomString = this.utilityService.returnLocalStorageData('userId');
  stockUserId = this.utilityService.returnLocalStorageData('stockUserId');
  betObj = {
    orderType: 'MARKET',
    qty: 1,
    limit: ''
  };
  allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'))

  usersPartnership :any;
  subscription;
  subscriptionDetail;
  subscriptionInstrument;
  detailInstrumentId;
  userId = this.utilityService.returnLocalStorageData('userId');
  marketList;
  m2mTotal;
  realisedPl;
  m2mreal;
  credit;
  equity;
  marginUsed;
  freeMargin;
  stockUsernamePrefix = env.stockUsernamePrefix();
  confirmDeleteData : any;
  endSubmit = false;
  @HostListener('focusin', ['$event']) public onListenerTriggered(event: any): void {
    if(event.sourceCapabilities) {
        //console.log("event")
    }else {
      this.setFocusToInput();
     }
  }
  ngOnInit() {

      let users = this.commonService.getLocalStorage();
      this.userLoginId = users.userId;
      this.socketService.joinRoom(this.userLoginId);
      this.socketServiceClient.joinRoom(this.userLoginId);
      if (this.utilityService.returnLocalStorageData('defultScript') == false || this.utilityService.returnLocalStorageData('allScript') == false) {
        setTimeout(() => {
          this.getAlldefaultScript();
          this.getAllExcahnge();
        }, 1000);
      }else{
          this.getAlldefaultScript();
          this.getAllExcahnge();
      }

      this.getAllOpenPosition();
      //this.getAllBalance();
      this.getAllTransactionForMarket();

      //Get latest transaction from socket
      this.socketServiceClient
        .getPlaceBetShareMarket()
        .subscribe((response) => {
          this.getAllTransactionForMarket();
          this.getAllOpenPosition();
        });


      this.socketServiceDotnetStock
      .receiveTradeData()
      .subscribe((response) => {
        if(response){
          this.completedOrder = _.filter(response , function (rest) {
            if(rest.OrderStatusText === 'Successful'){
              return rest;
            }
          });

          this.pendingOrder = _.filter(response , function (rest) {
            if(rest.OrderStatusText !== 'Successful'){
              return rest;
            }
          });
          console.log(this.pendingOrder.length);

        }
      });



    this.socketServiceDotnetStock
      .receiveTradeDataLive()
      .subscribe((response) => {
        if(response) {
          if (response.OrderStatusText == "Successful") {
            if (response.RefOrderId && response.RefOrderId != 0) {
              let findOrder = this.completedOrder.find(item => item.RefOrderId = response.OrderId);
              findOrder.OrderPrice = response.OrderPrice;
              findOrder.Quantity = response.Quantity;
            } else {
              this.completedOrder.push(response);
              this.pendingOrder = this.pendingOrder.filter(item => item.OrderNo != response.OrderNo);
            }
          } else {
            if (response.RefOrderId && response.RefOrderId != 0) {
              let findOrder = this.pendingOrder.find(item => item.RefOrderId = response.OrderId);
              findOrder.OrderPrice = response.OrderPrice;
              findOrder.Quantity = response.Quantity;
            } else {
              this.pendingOrder.push(response);
            }
          }
        }
        });

      this.socketServiceDotnetStock
        .receiveUserMarketStatus()
        .subscribe((response) => {
          this.allDeals = _.filter(response, function (rest) {
            console.log(rest);
            if (Number(rest.BuyQty) > 0) {
              rest.type = "Buy";
              rest.class = 'buy';
              rest.price = rest.BuyPrice;
              rest.quentity = rest.BuyQty;
            } else {
              rest.type = "Sell";
              rest.class = 'sell';
              rest.price = rest.SellPrice;
              rest.quentity = rest.SellQty;
            }
            let openQty = Number(rest.BuyQty) - Number(rest.SellQty);
            if(openQty < 0){
              rest.type  = "Sell";
              rest.class = 'sell';
              //rest.openQty = -1 * openQty;
            }else{
              rest.type = "Buy";
              rest.class = 'buy';
              //rest.openQty = openQty;
            }
            let openPrice = Number(rest.BuyPrice) - Number(rest.SellPrice);
            let openAvgPrice = openPrice / openQty;
            rest.openAvgPrice = openAvgPrice.toFixed(2);
            rest.openQty = openQty;
            rest.LotSize = Number(rest.LotSize);
            return rest;
          });
          let instrumentTokenAry = [];
          _.map(this.allDeals, function (e) {
            instrumentTokenAry.push(e.ScriptToken);
          });
          this.callSocketRate(instrumentTokenAry);
          this.getRate();
        });

        this.socketServiceDotnetStock
        .receiveUserBalanceStatus()
        .subscribe((response) => {
          this.marketStatus = response[0];
          console.log(response[0]['PL']);
          this.realisedPl = (this.marketStatus['PL'] + this.marketStatus['BK']).toFixed(2);
          console.log(this.realisedPl);
          this.credit = (this.marketStatus.CRD).toFixed(2);
          this.marginUsed = (this.marketStatus.MRGU).toFixed(2);
        });
  }



  ngOnDestroy() {

  }
  setFocusToInput() {

  }

  /**
   * @author TR
   * @date : 05-06-2020
   * get All Default Script
   */

  getAlldefaultScript(){
    this.spinner.show();
    let data = {
      userId: this.utilityService.returnLocalStorageData('userId'),
    };
    //  let defultScript = this.utilityService.returnLocalStorageData('defultScript')
    let defultScript = this.utilityService.returnLocalStorageData('defultScript');
    this.defaultScript = JSON.parse(defultScript);
    this.allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'));
    let totalRec = this.defaultScript.length;
    if(this.defaultScript != false){
      this.defaultScript.map((ite, index) =>{
        if(ite !== null) {
          let findScriptId =  this.allScriptDataAry.find(obj => obj.ScriptToken == ite.ScriptToken);
          if(findScriptId){
            ite.ScriptId = findScriptId.ScriptId,
            ite.ExchangeId = findScriptId.ExchangeId
          }
        this.instrumentTokenIds.push(ite.ScriptToken);
        if(totalRec - 1 === index){
            this.callSocketRate(this.instrumentTokenIds);
            this.getRate();
        }
        }
      });

    }
    this.spinner.hide();
  }


  callSocketRate(tokens){
    let uniqId = "";
    let tokenIds =  tokens.toString();
    //socket service market join
    var requested = {
                  "roomId": this.randomString,
                  "instrument_token": tokenIds,
                  "uniqId":this.commonService.getRandomString(10),
                  "response_type" : 'full'
                };
          // this.socketServiceBbMarket.leaveRoom(this.randomString);
          // this.socketServiceBbMarket.joinRoom(JSON.stringify(requested));
          if(this.subscription){
            this.clearSubscription(this.subscription);
          }
          var observable = interval(600);
          this.subscription = observable.subscribe(x => {
            this.socketServiceBbMarket.setInstrument(JSON.stringify(requested));
          });
  }

  clearSubscription(timer){
    let storeTimer = timer;
    setTimeout(function(){
      //storeTimer.unsubscribe();
    },3000);
  }




getRate(){
    this.socketServiceBbMarket
    .getInstrument()
    .subscribe((response) => {
      if(response.data.length > 0){
        if(response.roomId == "temp_" + this.userId){
          this.getDetailRate(response);
        }else if(response.roomId == "instrumentSocket_" + this.userId){
          this.getInstrumentRate(response);
        }else{
          response.data.map((result, index) =>{
          if(result){
            let data = result;

            if(this.allDeals){
              let findScriptId =  this.allDeals.find(obj => obj.ScriptToken == data.instrument_token);
              if(findScriptId){
                let currentPrice = (findScriptId.openQty>0)?data['buy_price_0']:data['sell_price_0'];
                let mtmpl = (currentPrice - Number(findScriptId.openAvgPrice)) * findScriptId.openQty * findScriptId.LotSize;
                // console.log("binary currentPrice+++++++++++++++",currentPrice);
                // console.log("binary stock market findScriptId.openAvgPrice+++++++++",findScriptId.openAvgPrice);
                // console.log("binary stock market findScriptId.openQty+++++++++",findScriptId.openQty);
                // console.log("binary stock market findScriptId.LotSize+++++++++",findScriptId.LotSize);

                // console.log("mtmpl++++++++++++",mtmpl);
                findScriptId.profileAndLoss = mtmpl;
                $("#" + data.instrument_token + "_cmp").html(String(currentPrice));
                $("#" + data.instrument_token + "_mmpl").html(String(mtmpl.toFixed(2)));
                $("#" + data.instrument_token + "_cmp_mob").html(String(currentPrice));
                $("#" + data.instrument_token + "_mmpl_mob").html(String(mtmpl.toFixed(2)));
                this.updateTotal();
              }
            }
            $("#" + data.instrument_token + "_buy_price_0").html(data['buy_price_0']);
            $("#"+  data.instrument_token + '_buy_price_0_netChange').html(data['buy_price_0']);
            $("#"+  data.instrument_token + '_buy_price_0_netChange_mob').html(data['buy_price_0']);

           $("#"+  data.instrument_token + '_sell_price_0_netChange').html(data['sell_price_0']);
            $("#"+  data.instrument_token + '_sell_price_0_netChange_mob').html(data['sell_price_0']);
            let preAmntBuy = $("#prev_" + data.instrument_token + "_buy_price_0").val();
            if(Number(data['buy_price_0']) > Number(preAmntBuy)){
              $("#" + data.instrument_token  + "_buy_price_0").removeClass('debit-color').addClass('credit-color');
              $("#" + data.instrument_token + "_buy_price_0_mob").removeClass('debit-color').addClass('credit-color');
            }
            if(Number(data['buy_price_0']) < Number(preAmntBuy)){
              $("#" + data.instrument_token  + "_buy_price_0").removeClass('credit-color').addClass('debit-color');
              $("#" + data.instrument_token + "_buy_price_0_mob").removeClass('credit-color').addClass('debit-color');
            }
            $("#prev_" + data.instrument_token + "_buy_price_0").val(data['buy_price_0']);


            let preAmntSell = $("#prev_" + data.instrument_token + "_sell_price_0").val();

            if(Number(data['sell_price_0']) > Number(preAmntSell)){
              $("#" + data.instrument_token  + "_sell_price_0").removeClass('debit-color').addClass('credit-color');
              $("#" + data.instrument_token + "_sell_price_0_mob").removeClass('debit-color').addClass('credit-color');
            }

            if(Number(data['sell_price_0']) < Number(preAmntSell)){
              $("#" + data.instrument_token  + "_sell_price_0").removeClass('credit-color').addClass('debit-color');
              $("#" + data.instrument_token + "_sell_price_0_mob").removeClass('credit-color').addClass('debit-color');
            }

            $("#prev_" + data.instrument_token + "_sell_price_0").val(data['sell_price_0']);

            $("#" + data.instrument_token + "_sell_price_0").html(data['sell_price_0']);
            $("#" + data.instrument_token + "_last_price").html(data['last_price']);
            $("#" + data.instrument_token + "_net_change").html(data['net_change']);
            $("#" + data.instrument_token + "_ohlc_high").html(data['ohlc_high']);
            $("#" + data.instrument_token + "_ohlc_low").html(data['ohlc_low']);
            $("#" + data.instrument_token + "_last_trade_time").val(data['last_trade_time']);

            $("#" + data.instrument_token + "_buy_price_0_mob").html(data['buy_price_0']);
            $("#" + data.instrument_token + "_sell_price_0_mob").html(data['sell_price_0']);
            $("#" + data.instrument_token + "_last_price_mob").html(data['last_price']);
            $("#" + data.instrument_token + "_net_change_mob").html(data['net_change']);
            $("#" + data.instrument_token + "_ohlc_high_mob").html(data['ohlc_high']);
            $("#" + data.instrument_token + "_ohlc_low_mob").html(data['ohlc_low']);
            $("#" + data.instrument_token + "_last_trade_time_mob").html(data['last_trade_time']);

            if(data['net_change'] >0){
              $("#"+data.instrument_token + ' ' + 'i').removeClass('fa fa-arrow-down down-arrow');
              $("#"+data.instrument_token + ' ' + 'i').addClass('fa fa-arrow-up up-arrow');
              // $("#"+data.instrument_token + '_mob').removeClass('debit-border');
              // $("#"+data.instrument_token + '_mob').addClass('credit-border');
              $("#"+data.instrument_token + '_net_change_mob').removeClass('debit-color');
              $("#"+data.instrument_token + '_net_change_mob').addClass('credit-color');
            }else{
              $("#"+data.instrument_token + ' ' + 'i').removeClass('fa fa-arrow-up up-arrow');
              $("#"+data.instrument_token + ' ' + 'i').addClass('fa fa-arrow-down down-arrow');
              // $("#"+data.instrument_token + '_mob').removeClass('credit-border');
              // $("#"+data.instrument_token + '_mob').addClass('debit-border');
              $("#"+data.instrument_token + '_net_change_mob').removeClass('credit-color');
              $("#"+data.instrument_token + '_net_change_mob').addClass('debit-color');
            }
          }
          });
        }
      }
    });
  }

getDetailRate(response){
  if(response){
      let res = response.data[0];
      if(res.instrument_token == this.detailInstrumentId){
        $("#" + res.instrument_token +  "_ohlc_open_single").html(res.ohlc_open);
        $("#" + res.instrument_token +  "_ohlc_close_single").html(res.ohlc_close);
        $("#" + res.instrument_token +  "_ohlc_high_bet_single").html(res.ohlc_high);
        $("#" + res.instrument_token +  "_ohlc_low_bet_single").html(res.ohlc_low);
        $("#" + res.instrument_token +  "_last_price_bet_single").html(res.last_trade_time.substring(20, res.last_trade_time.lastIndexOf(" ")+1));
        $("#" + res.instrument_token +  "_volume_single").html(res.volume);
        $("#"+res.instrument_token+"_buy_price_0_single_input").val(res.buy_price_0);
        $("#"+res.instrument_token+"_sell_price_0_single_input").val(res.sell_price_0);
        $("#"+res.instrument_token+"_volume_single_input").val(res.volume);

        $("#"+res.instrument_token+"_buy_price_0_single").html(res.buy_price_0);
        $("#"+res.instrument_token+"_sell_price_0_single").html(res.sell_price_0);
        this.spinner.hide();
        let preAmntBuy = $("#prev_" + res.instrument_token + "_buy_price_0").val();

        if(res.buy_price_0 > preAmntBuy){
          $("#" + res.instrument_token  + "_buy_price_0_single").removeClass('red').addClass('green'); //use current element
        }else{
          $("#" + res.instrument_token  + "_buy_price_0_single").removeClass('green').addClass('red'); //use current element
        }

        let preAmntSell = $("#prev_" + res.instrument_token + "_sell_price_0").val();
        if(res.sell_price_0 > preAmntSell){
          $("#" + res.instrument_token  + "_sell_price_0_single").removeClass('red').addClass('green'); //use current element
        }else{
          $("#" + res.instrument_token  + "_sell_price_0_single").removeClass('green').addClass('red'); //use current element
        }
      }
  }
}

getInstrumentRate(response){

  response.data.map((result, index) =>{
    if(result){
      let data = result;
      $("#"+  data.instrument_token + '_buy_price_0_netChange').html(data['buy_price_0']);
      $("#"+  data.instrument_token + '_buy_price_0_netChange_mob').html(data['buy_price_0']);

     $("#"+  data.instrument_token + '_sell_price_0_netChange').html(data['sell_price_0']);
      $("#"+  data.instrument_token + '_sell_price_0_netChange_mob').html(data['sell_price_0']);
        let sellRate = $("#" + data.instrument_token + "_buy_price_0_netChange").html();
            let buyRate = $("#" + data.instrument_token + "_sell_price_0_netChange").html();
            let averPrice = $("#" + data.instrument_token + "_averagePrice").html();
            let openQny = $("#" + data.instrument_token + "_openQuantity").html();
            let lotSize = $("#" + data.instrument_token + "_lotSize").html();
            let type = $("#" + data.instrument_token + "_type").html();


            if(type === 'SELL'){
              let TotalCal = ((Number(averPrice)  - Number(buyRate)) * Number(openQny)) * Number(lotSize);
              let TotalValue = TotalCal.toFixed(2);
              $("#"+data.instrument_token +'_sell_price_0_netChange1').html(String(TotalValue));
              $("#"+data.instrument_token + '_sell_price_0_netChange1_mob').html(String(TotalValue));
              if(Number(TotalValue) > 0){
                $("#" + data.instrument_token  +  '_sell_price_0_netChange1').removeClass('red-color').addClass('green-color'); //use current element
                $("#" + data.instrument_token  + '_sell_price_0_netChange1_mob').removeClass('red-color').addClass('green-color'); //use current element
              }else{
                $("#" + data.instrument_token  +  '_sell_price_0_netChange1').removeClass('green-color').addClass('red-color'); //use current element
                $("#" + data.instrument_token  + '_sell_price_0_netChange1_mob').removeClass('green-color').addClass('red-color'); //use current element
              }
            } else {
              let TotalCal = ((Number(sellRate)  - Number(averPrice)) * Number(openQny)) * Number(lotSize);

              let TotalValue = TotalCal.toFixed(2);

              $("#"+data.instrument_token + '_sell_price_0_netChange1').html(String(TotalValue));
              $("#"+data.instrument_token + '_sell_price_0_netChange1_mob').html(String(TotalValue));
              if(Number(TotalValue) > 0){
                $("#" + data.instrument_token  + '_sell_price_0_netChange1').removeClass('red-color').addClass('green-color'); //use current element
                $("#" + data.instrument_token  + '_sell_price_0_netChange1_mob').removeClass('red-color').addClass('green-color'); //use current element
              }else{
                $("#" + data.instrument_token  + '_sell_price_0_netChange1').removeClass('green-color').addClass('red-color'); //use current element
                $("#" + data.instrument_token  + '_sell_price_0_netChange1_mob').removeClass('green-color').addClass('red-color'); //use current element
              }
            }
    }
  });
}

updateTotal(){
  let sum = 0
  _.filter(this.allDeals, function(item) {
    if(item.profileAndLoss){
      sum = sum + item.profileAndLoss;
    }
  });
  this.m2mTotal = sum.toFixed(2);
  this.m2mreal = (Number(this.m2mTotal) + Number(this.realisedPl)).toFixed(2);
  this.equity =  (Number(this.m2mreal) + Number(this.credit)).toFixed(2);
  this.freeMargin = (Number(this.equity) - Number(this.marginUsed)).toFixed(2);
}

  openScriptModal(){
     this.instrumentList();
     this.modal.show();
  }

  betModal(data,edit=false){
    this.openBetModal.show();
    data.ScriptToken = String(data.ScriptToken);

   // this.spinner.show();
    // console.log(data);
    // console.log(edit);
    data.edit = edit;
    if(edit == true){
      this.betObj.orderType = data.OrderTypeText;
      this.betObj.limit =  data.OrderPrice;
      this.betObj.qty = data.Quantity;
    }else{
      if(data.quentity){
        this.betObj.qty = data.quentity;
      }
    }

    let tempInst = "temp_" + this.userId;
    this.detailInstrumentId = data.ScriptToken;
    //this.socketServiceBbMarket.leaveRoom(tempInst);

    var requested = {
      "roomId": tempInst,
      "instrument_token": data.ScriptToken,
      "uniqId":this.commonService.getRandomString(10),
      "response_type" : 'full'
    };
    console.log(requested);
    if(this.subscriptionDetail){
      this.clearSubscription(this.subscriptionDetail);
    }
    var observable = interval(600);
    this.subscriptionDetail = observable.subscribe(x => {
      this.socketServiceBbMarket.detailInstrument(JSON.stringify(requested));
      // this.spinner.hide();
    });

    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          let obj = {
            ScriptId: data.ScriptId,
            accessToken : responseToken.data.access_token,
            UserId : this.stockUserId
          }
            this.binaryService.orderTypeList(obj).subscribe(orderScriptRes => {
              if(orderScriptRes.result == 1){
                this.marketList = orderScriptRes.data;
                if(edit == true){
                  this.marketList = _.filter(this.marketList, function(item) {
                    console.log(item.OrderTypeName);
                    console.log(data.OrderTypeText);

                    if(item.OrderTypeName == data.OrderTypeText){
                      item.selected = true
                    }
                    return item;
                  });
                }
              }
            });
      }else{
        this.utilityService.userLogout();
      }
    })


    let buy_price_0 =  $("#" + data.ScriptToken + "_placeBetReq").data('data-buy_price_0');

    // this.setFocusToInput()
    this.informationData = data;
    this.informationData.buy_price_0 = buy_price_0;

    // this.instrumentList();

  }




  closeinfoModel(data){
    if(this.subscriptionDetail){
      this.clearSubscription(this.subscriptionDetail);
    }
    $('#' + data.ScriptToken +'_buy_price_0_single').text('-');
    $('#' + data.ScriptToken +'_sell_price_0_single').text('-');
    this.betObj.orderType = 'MARKET';
    this.betObj.qty = 1;
    this.betObj.limit = '';

    this.informationData.name = '';
    this.informationData.expiry = '';
    this.informationData.lotSize = '';
    this.informationData.tradeMargin = '';
    this.informationData.tradeAttribute = '';
    this.informationData.quantityMax = '';
    this.informationData.quantityBreakup = '';
    this.informationData.maxLot = '';
    this.informationData.breakUpLot = '';


    this.openBetModal.hide();
    //this.infoModal.hide();
  }
  closeinfoModelConfirm(){
    this.confrimModal.hide();
  }

  deleteScriptCon(){

  }

  getAllExcahnge(){
    let myExchRes = this.utilityService.returnLocalStorageData('myExchRes');
    this.getAllExchng = JSON.parse(myExchRes);
  }
  instrumentList() {
    let defualtData = this.utilityService.returnLocalStorageData('myScript');
    let defualtDataAry = JSON.parse(defualtData)
    let defultScript = this.utilityService.returnLocalStorageData('defultScript');
    let defultScriptAry = JSON.parse(defultScript);
    this.addDefaultData = [];
      this.addDefaultData =  _.filter(defualtDataAry, function(item) {
        if(defultScriptAry != false){
          let findIndex =  defultScriptAry.findIndex(obj => obj.ScriptToken == item.ScriptToken);
          if(findIndex != -1){
            item.scriptAdded = true;
          }else{
            item.scriptAdded = false;
          }
          return item;
        }else{
          item.scriptAdded = false;
          return item;
        }
     });
   }





  /**
   * @author TR_buy_price_0_single_inputvalue
   */

  onSelectionChange(e){
    this.selectedValue = e;
    if(e != 'ALL'){
      this.filterDataObjects = _.filter(this.addDefaultData, function(item) {
        if(item.ExchangeName == e){
          return item;
        }
      });

      if (this.filterDataObjects && this.filterDataObjects.length > 0) {
        this.filterData = true;
      } else {
        this.filterData = false;
      }
    }else{
      this.filterData = false
    }
  }


  /**
   * @author TR
   * @date : 12-06-2020
   * check list add portfolio
   */

  check(e, data) {
    if(this.endSubmit) {
        return;
    }
    this.endSubmit = true;
    this.spinner.show();
    let key = env.constantKey();
    let defultPortfolio = this.utilityService.returnLocalStorageData('portfolio')
    defultPortfolio = JSON.parse(defultPortfolio)
      //Added script into api
      this.userService.getNewToken().subscribe(responseToken => {
        if(responseToken.status == true){
          let refreshToken = responseToken.data.refresh_token;
            this.utilityService.storeRefreshToken(refreshToken);
            this.endSubmit = false;
            let obj = {
              PortfolioIndex: defultPortfolio.PortfolioIndex,
              ScriptToken: data.ScriptToken,
              CreatedBy: Number(this.utilityService.returnLocalStorageData('stockUserId')),
              accessToken : responseToken.data.access_token,
              UserId : Number(this.stockUserId)
            }
            if (e.target.checked === true) {
              delete obj.UserId;
              this.binaryService.assignSingleScript(obj).subscribe(myExchRes => {
                if(myExchRes.result == 1){
                    let addObj = {
                      PortfolioIndex : defultPortfolio.PortfolioIndex,
                      PortfolioName : defultPortfolio.PortfolioName,
                      ScriptToken : data.ScriptToken,
                      Script : data.Script,
                      ExpiryDate : data.ExpiryDate,
                      ExchangeName : data.ExchangeName,
                      scriptAdded : true,
                      ScriptId :   data.ScriptId,
                      ExchangeId : data.ExchangeId
                    }
                  this.spinner.hide();
                    let defultScript = this.utilityService.returnLocalStorageData('defultScript');
                    let defultScriptAry = JSON.parse(defultScript);
                    if(defultScriptAry != false){
                      let findIndex =  defultScriptAry.findIndex(obj => obj.ScriptToken == data.ScriptToken);
                      if(findIndex == -1){
                        defultScriptAry.push(addObj);
                        var clientPortIndexEnc = aes256.encrypt(key, JSON.stringify(defultScriptAry));
                        localStorage.setItem('defultScript', clientPortIndexEnc);
                      }
                    }else{
                      defultScriptAry = [];
                      defultScriptAry.push(addObj);
                      var clientPortIndexEnc = aes256.encrypt(key, JSON.stringify(defultScriptAry));
                      localStorage.setItem('defultScript', clientPortIndexEnc);
                    }
                    console.log(this.defaultScript);
                    if(this.defaultScript == false){
                      this.defaultScript = [];
                    }
                    this.defaultScript.push(addObj);
                    let totalRec = this.defaultScript.length;
                    this.defaultScript.map((ite, index) =>{
                      if(ite !== null) {
                      this.instrumentTokenIds.push(ite.ScriptToken);
                      if(totalRec - 1 === index){
                          this.callSocketRate(this.instrumentTokenIds);
                          this.getRate();
                      }
                      }
                    });
                }
              }, error => {
                //this.utilityService.userLogout();
                // this.utilityService.popToast('error','Error', 3000 , error);
              });
            }else{
              delete obj.CreatedBy;
              this.binaryService.removeSingleScript(obj).subscribe(myExchRes => {
                if(myExchRes.result == 1){
                  this.spinner.hide();
                  let defultScript = this.utilityService.returnLocalStorageData('defultScript');
                  let defultScriptAry = JSON.parse(defultScript);
                  let findIndex =  defultScriptAry.findIndex(obj => obj.ScriptToken == data.ScriptToken);
                  if(findIndex != -1){
                    // defultScriptAry.push(addObj);
                    defultScriptAry = defultScriptAry.filter(function( obj ) {
                      return obj.ScriptToken !== data.ScriptToken;
                    });
                    console.log(defultScriptAry);
                    var clientPortIndexEnc = aes256.encrypt(key, JSON.stringify(defultScriptAry));
                    localStorage.setItem('defultScript', clientPortIndexEnc);
                    this.defaultScript = defultScriptAry;
                  }
                }
              }, error => {
               // this.utilityService.userLogout();
                // this.utilityService.popToast('error','Error', 3000 , error);
              });
            }
          }else{
            this.utilityService.userLogout();
          }
        });
  }


  /**
   * @author TR
   * @date : 12-06-2020
   * Local Search Functionality
   */

  mySearch(){
    const x = this.searchVal.toLowerCase();
    this.filterDataObjects = _.filter(this.addDefaultData, function(item) {
      if (item.Script.toLowerCase().indexOf(x) !== -1) {
        return item;
      }
    });

    if (this.filterDataObjects && this.filterDataObjects.length > 0) {
      this.filterData = true;
    } else {
      this.filterData = false;
    }
  }

  getExchangeDetail(data){
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          let marObj = {
            ScriptId : data.ScriptId,
            userId: Number(this.utilityService.returnLocalStorageData('stockUserId')),
            accessToken : responseToken.data.access_token
          };
          this.binaryService.marketWatchDetails(marObj).subscribe(orderScriptRes => {
            if(orderScriptRes.result == 1){
              if(orderScriptRes.lstMarketWatchDetails.length > 0){
                let detailObj = orderScriptRes.lstMarketWatchDetails[0];
                this.informationData.name = detailObj.Script;
                this.informationData.expiry = detailObj.ExpiryDate;
                this.informationData.lotSize = detailObj.LotSize;
                this.informationData.tradeMargin = detailObj.TradeMargin;
                this.informationData.tradeAttribute = detailObj.TradeAttribute;
                this.informationData.quantityMax = detailObj.MaxQty;
                this.informationData.quantityBreakup = detailObj.BreakUpQty;
                this.informationData.maxLot = detailObj.MaxLot;
                this.informationData.breakUpLot = detailObj.BreakUpLot;
              }
            }
          });
      }else{
        this.utilityService.userLogout();
      }
    });
 }


  closeModel(){
  this.modal.hide();
  }

  /**
   * @author TR
   * @date : 20-07-2020
   * placebet
   * @method: POST
   */

  marketPlaceBet(data , type) {
    let Quantity = this.betObj.qty;
    let buyPrice = $('#' + data.ScriptToken + '_buy_price_0_single_input').val();
    let sellPrice = $('#' + data.ScriptToken + '_sell_price_0_single_input').val();
    let Volume = $('#' + data.ScriptToken + '_volume_single_input').val();

    let ohlc_high = $('#' + data.ScriptToken + '_ohlc_high').text();
    let ohlc_low = $('#' + data.ScriptToken + '_ohlc_low').text();

    if(this.betObj.orderType == "Limit"){
      let checkPrice =  /^[1-9][\.\d]*(,\d+)?$/.test(this.betObj.limit);
      if(checkPrice==false){
        this.utilityService.popToast('error','Error', 3000 ,"Enter valid price");
        return false;
      }
    }
    sellPrice = String(sellPrice);
    buyPrice = String(buyPrice);

    let orderObj = {
      OrderPrice : '',
      CurrentMapPrice : '',
      ExecutionPrice : '',
      ReferencePrice :''
    };
    console.log(this.betObj.orderType);
    if(this.betObj.orderType == "MARKET"){
      if(type=="BUY"){
          orderObj.OrderPrice = sellPrice;
          orderObj.CurrentMapPrice = sellPrice;
          orderObj.ExecutionPrice = sellPrice;
          orderObj.ReferencePrice = sellPrice
      }else{
          orderObj.OrderPrice = buyPrice;
          orderObj.CurrentMapPrice = buyPrice;
          orderObj.ExecutionPrice = buyPrice;
          orderObj.ReferencePrice = buyPrice
      }
      ohlc_high = '';
      ohlc_low = '';
    }else if(this.betObj.orderType == "Limit"){
      if(type=="BUY"){
        if(Number(this.betObj.limit) < Number(sellPrice)){
          orderObj.OrderPrice = this.betObj.limit;
          ohlc_high = ohlc_high;
          ohlc_low = ohlc_low;
          orderObj.CurrentMapPrice = sellPrice;
          orderObj.ExecutionPrice = sellPrice;
          orderObj.ReferencePrice = sellPrice;
        }else{
          this.utilityService.popToast('error','Error', 3000 ,"Please enter valid price.");
          return false;
        }
      }else{
        if(Number(this.betObj.limit) > Number(buyPrice)){
          orderObj.OrderPrice = this.betObj.limit;
          ohlc_high = ohlc_high;
          ohlc_low = ohlc_low;
          orderObj.CurrentMapPrice = buyPrice;
          orderObj.ExecutionPrice  = buyPrice;
          orderObj.ReferencePrice  = buyPrice;
        }else{
          this.utilityService.popToast('error','Error', 3000 ,"Please enter valid price.");
          return false;
        }
      }
    }else{
      if(type=="BUY"){
        if(Number(this.betObj.limit) > Number(sellPrice)){
          orderObj.OrderPrice = this.betObj.limit;
          ohlc_high = ohlc_high;
          ohlc_low = ohlc_low;
          orderObj.CurrentMapPrice = sellPrice;
          orderObj.ExecutionPrice = sellPrice;
          orderObj.ReferencePrice = sellPrice;
        }else{
          this.utilityService.popToast('error','Error', 3000 ,"Please enter valid price.");
          return false;
        }
      }else{
        if(Number(this.betObj.limit) < Number(buyPrice)){
          orderObj.OrderPrice = this.betObj.limit;
          ohlc_high = ohlc_high;
          ohlc_low = ohlc_low;
          orderObj.CurrentMapPrice = buyPrice;
          orderObj.ExecutionPrice  = buyPrice;
          orderObj.ReferencePrice  = buyPrice;
        }else{
          this.utilityService.popToast('error','Error', 3000 ,"Please enter valid price.");
          return false;
        }
      }
    }

    if(Quantity <= 0){
      this.utilityService.popToast('error','Error', 3000 ,"Enter valid quentity");
      return false;
    }
    this.spinner.show();
    data.instrumentToken = data.ScriptToken;


    let last_trade_time = $('#' + data.instrumentToken + '_last_trade_time').val();


    let date = moment(last_trade_time, 'DD-MM-YYYY hh:mm:ss')
    last_trade_time = date.format('YYYY-MM-DD hh:mm:ss');

    let UserId = Number(this.utilityService.returnLocalStorageData('stockUserId'));
    let ExchangeId = (data.ExchangeId)? data.ExchangeId : data.ExchangeID;
    let ScriptId = data.ScriptId;
    let OrderType = (this.betObj.orderType == "MARKET")?'M':(this.betObj.orderType == "Limit") ? 'RL' : 'SL';
    let BuySell = (type=="BUY")?'B':'S';
    let OrderStatus = 'P';
    let IPAddress = this.utilityService.returnLocalStorageData('ip');



    this.usersPartnership = this.commonService.getLocalStorage();
    this.usersPartnership = JSON.parse(this.usersPartnership.userData);

    let userData = this.utilityService.returnLocalStorageData('userData');
    userData = JSON.parse(userData);

    let placeReq = {
        UserId:UserId,
        ExchangeId:String(ExchangeId),
        ScriptId:String(ScriptId),
        OrderType:OrderType,
        Quantity:String(Quantity),
        BuySell:BuySell,
        OrderPrice:orderObj.OrderPrice,
        CurrentMapPrice:orderObj.CurrentMapPrice,
        LowMapPrice: ohlc_low,
        HighMapPrice:ohlc_high,
        ExecutionPrice: orderObj.ExecutionPrice,
        Volume:String(Quantity),
        // OrderTime:currentDate,
        OrderStatus:OrderStatus,
        // TradedBy:UserId,
        // Status:"A",
        // CreatedBy:UserId,
        // IPAddress:IPAddress,
        DeviceId:"Web",
        UserName: userData.userName + this.stockUsernamePrefix ,
        ReferencePrice:orderObj.ReferencePrice,
        OrderMethod:'M',
        accessToken : '',
        ScriptToken : data.ScriptToken
    }
    if(data.OrderId){
      placeReq['RefOrderId'] = data.OrderId
    }
    if(this.betObj.orderType != "MARKET"){
      placeReq['LastTradeTime'] = last_trade_time
    }
    if(this.subscriptionDetail){
      this.clearSubscription(this.subscriptionDetail);
    }
    this.openBetModal.hide();
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          placeReq.accessToken = responseToken.data.access_token
          this.binaryService.placeBetExternal(placeReq).subscribe(response => {
            if(response.result == 1){
              let objResponseBO =  response.objResponseBO;
              let orderId =  objResponseBO.ResponseData.OrderId;
              let obj = {
                OrderId : orderId,
                UserId : UserId
              }
              this.socketServiceDotnetStock.placeOrder(obj); // socket join room
              this.socketServiceDotnetStock.userMarketStatus(UserId); // socket emit
              this.socketServiceDotnetStock.userBalanceStatus(UserId); // socket emit

              this.betObj.orderType = 'MARKET';
              this.betObj.qty = 1;
              this.betObj.limit = '0';

              // this.playAudio();
              this.utilityService.popToast('success', 'Success', 1500, type + " ORDER SUCCESS.");
              this.spinner.hide();
            }else{
              let objResponseBO =  response.objResponseBO;
              this.betObj.orderType = 'MARKET';
              this.betObj.qty = 1;
              this.betObj.limit = '0';
              this.spinner.hide();
              this.utilityService.popToast('error','Error', 3000 ,objResponseBO.ResponseMessage);
            }
          }, error => {
            this.betObj.orderType = 'MARKET';
            this.betObj.qty = 1;
            this.betObj.limit = '0';
            this.spinner.hide();
            this.utilityService.popToast('error','Error', 3000 , error);
          });
      }else{
        this.utilityService.userLogout();
      }
    });
  }


  getAllTransactionForMarket(){


let weekData = this.utilityService.getCurrentWeek();
let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
console.log(weekData);
this.socketServiceDotnetStock.joinRoom("mobile-user-room-" + userId); // socket join room

let data = {
      LoginUserId : userId,
      RoomId : "mobile-user-room-" + userId,
      FromDate : weekData.fromDate,
      ToDate : weekData.toDate,
      OrderStatus : "S,P",
      PageNumber : 1,
      PageSize : 50,
      UserId: "",
      ExchangeId: "",
      ScriptId: ""
    }
    this.socketServiceDotnetStock.requestTrade(data); // socket emit
    this.socketServiceDotnetStock.userMarketStatus(userId); // socket emit
    this.socketServiceDotnetStock.userBalanceStatus(userId); // socket emit

  }

  getAllOpenPosition(){
    let data = {
      userId : this.utilityService.returnLocalStorageData('userId')
    };
    this.binaryService.getAllOpenPosition(data).subscribe(response => {
      let instrumentTokenAry = [];
      this.openPositionResponse = response.data;
      this.openPositionResponse.map((ite, index) =>{
        instrumentTokenAry.push(ite.instrumentToken);
      });
      let tokenIds =  instrumentTokenAry.toString();
      var requested = {
        "roomId": "instrumentSocket_" + this.userId,
        "instrument_token": tokenIds,
        "uniqId":this.commonService.getRandomString(10),
        "response_type" : 'full'
      };
      if(this.subscription){
        this.clearSubscription(this.subscriptionInstrument);
      }
      var observable = interval(600);
      this.subscriptionInstrument = observable.subscribe(x => {
        this.socketServiceBbMarket.setInstrument(JSON.stringify(requested));
      });
      this.getRate();
      // this.utilityService.popToast('success','Success', 3000 , 'Delete default successfully.');
      if(this.openPositionResponse.length > 0){
        setInterval(function()
          {
            const cls = document.getElementById('#MarketWatchM2M').getElementsByTagName('td');
            var sum = 0;
            for (var i = 0; i < cls.length; i++){
              if(cls[i].className === 'rate-td width-25 text-center f-12 l-h-33 countableSec red-color'){
                sum += isNaN(Number(cls[i].innerHTML)) ? 0 : Number(cls[i].innerHTML);
              } else if (cls[i].className === 'rate-td width-25 text-center f-12 l-h-33 countableSec green-color') {
                sum += isNaN(Number(cls[i].innerHTML)) ? 0 : Number(cls[i].innerHTML);
              }
            }
            let balance = $('#balanceCl').text();

            let checkVal = Math.sign(sum);
            if(checkVal > 0) {
              const pl = Number(balance) + sum;
              const totalPlPos = String(pl.toFixed(2));
              $('#netPL').html(totalPlPos);
              $('#m2mTotal').removeClass('red-color').addClass('green-color');
              $('#netPL').removeClass('red-color').addClass('green-color');

              //Mob view
              $('#netPLMob').html(totalPlPos);
              $('#m2mTotalMob').removeClass('red-color').addClass('green-color');
              $('#netPLMob').removeClass('red-color').addClass('green-color');
            } else {
              const pls = Number(balance) + sum;
              const totalPlNG = String(pls.toFixed(2));
              $('#netPL').html(totalPlNG);
              $('#m2mTotal').removeClass('green-color').addClass('red-color');
              $('#netPL').removeClass('green-color').addClass('red-color');

              // Mobile View
              $('#netPLMob').html(totalPlNG);
              $('#m2mTotalMob').removeClass('green-color').addClass('red-color');
              $('#netPLMob').removeClass('green-color').addClass('red-color');
            }
            let total = String(sum.toFixed(2));
            $('#m2mTotal').html(total);
            $('#m2mTotalMob').html(total);
          },
          3000);
      }
      this.spinner.hide();
    }, error => {
      // this.utilityService.popToast('error','Error', 3000 , error);
    });
  }
  playAudio(){
    let audio = new Audio();
    audio.src = '../../../assets/audio/notification-1.ogg';
    audio.load();
    audio.play();
  }

  openConfirmModal(data){
    this.confrimModal.show();
    this.confirmDeleteData = data;
  }

  deletePending(){
    this.confrimModal.hide();
    let data = this.confirmDeleteData;
    this.confirmDeleteData = {};
    this.spinner.show();

    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          let obj = {
            UserId:data.UserId,
            OrderId:data.OrderId,
            OrderStatus:data.OrderStatus,
            accessToken : responseToken.data.access_token
          }
            this.binaryService.DeletePendingReport(obj).subscribe(orderScriptRes => {
              if(orderScriptRes.result == 1){
                this.spinner.hide();
                console.log(data.OrderTypeText);
                if(data.OrderTypeText == 'Limit' || data.OrderTypeText == 'StopLoss'){
                  this.pendingOrder = this.pendingOrder.filter((item) => item.OrderId != data.OrderId);
                }
              }else{
                this.spinner.hide();
              }
            });
      }else{
        // this.utilityService.userLogout();
        this.utilityService.popToast('error','Error', 3000 , 'Something went wrong.');
        this.spinner.hide();
      }
    });
  }

  editPending(data){
      console.log(data);
      this.betModal(data,true);
  }

  onOptionsSelected(selectedType){
    if(selectedType == 'M'){
      this.betObj.orderType  = 'MARKET'
    }
    if(selectedType == 'RL'){
      this.betObj.orderType = 'Limit'
    }
    if(selectedType == 'SL'){
      this.betObj.orderType = 'StopLoss';
    }
  }

  openmarket(maketName) {
    if (maketName === 'Quotes') {
        $('#Position').css("display", "none");
        $('#Trade').css('display', 'none');
        $('#Quotes').css('display', 'block');
    }
    if (maketName === 'Trade') {
        $('#Quotes').css('display', 'none');
        $('#Position').css('display', 'none');
        $('#Trade').css('display', 'block');
    }
    if (maketName === 'Position') {
        $('#Quotes').css('display', 'none');
        $('#Trade').css('display', 'none');
        $('#Position').css('display', 'block');
    }
    $('.bottom-menu-icon-main').click(function(e) {
        $('.bottom-menu-icon-main').removeClass('active-color1');
        $(this).addClass('active-color1');
    });
}
}

