import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryViewComponent } from './binary-view.component';

describe('BinaryViewComponent', () => {
  let component: BinaryViewComponent;
  let fixture: ComponentFixture<BinaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
