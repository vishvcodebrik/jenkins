import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SportsComponent } from './sports.component';
import { ListViewComponent } from './list-view/list-view.component';
import { CardViewComponent } from './card-view/card-view.component';
import { GameViewComponent } from './game-view/game-view.component';
import {AuthGuard} from "../auth-gaurd/auth-guard.service";
import {BinaryViewComponent} from "./binary-view/binary-view.component";
import {CasinoViewComponent} from './casino-view/casino-view.component';


const routes: Routes = [{
  path: 'sports',
  component: SportsComponent,
  children: [
      {
          path: 'view',
        canActivate: [AuthGuard],
          component: ListViewComponent,
      },
      {
        path: 'card-view',
        canActivate: [AuthGuard],
        component: CardViewComponent,
      },
      {
        path: 'game-view/:id',
        canActivate: [AuthGuard],
        component: GameViewComponent,
    },
      {
        path: 'binary-view',
        canActivate: [AuthGuard],
        component: BinaryViewComponent,
    },
    {
        path: 'casino-view/:id',
        canActivate: [AuthGuard],
        component: CasinoViewComponent,
    },
      {
        path: 'event-view',
        canActivate: [AuthGuard],
        component: ListViewComponent,
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SportsRoutingModule { }
