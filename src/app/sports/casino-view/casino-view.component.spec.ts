import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasinoViewComponent } from './casino-view.component';

describe('CasinoViewComponent', () => {
  let component: CasinoViewComponent;
  let fixture: ComponentFixture<CasinoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasinoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasinoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
