import {Component, OnInit, Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs';
import * as env from '../globals/env';
import {NgxSpinnerService} from 'ngx-spinner';


declare var $: any;

@Injectable()

export class SocketServiceBbMarket {
  public getBbMarketRate = (id) => {
    return Observable.create((observer) => {
      this.socket.on('api-data', (message) => {
        observer.next(message);
      });
    });
  }
  private url = 'https://sslmkt.rdsconn.com/';
  private socket;

  constructor(@Inject(DOCUMENT) private document: any,
              private router: Router,
              private spinner: NgxSpinnerService,
              private location: Location) {

    this.socket = io(this.url, {
        query: {
          api_token_temp: 'android'
        },
        autoConnect: true,
        reconnection: true,
        reconnectionDelay: 500,
        reconnectionDelayMax: 1000,
        reconnectionAttempts: Infinity,
        transports: ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling']
      }
    );
    //this.socket.emit('join-room', 'room2');
  }

  //Join room function
  joinRoom(roomId) {
    //console.log(roomId);
    this.socket.emit('join-room', roomId);
  }

  disconnect(){
    this.socket.disconnect(0);
  }

  connect(){
    this.socket.connect(0);
  }


  setInstrument(roomId){
      this.socket.emit('set-instrument', roomId);
  }

  detailInstrument(roomId){
    this.socket.emit('set-instrument', roomId);
  }

  public getInstrument = () => {
    return Observable.create((observer) => {
        this.socket.on('get-instrument', (message) => {
            observer.next(message);
        });
    });
  }
}
