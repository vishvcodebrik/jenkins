'use strict';
import {isDevMode} from '@angular/core';


// This is url to get data from office panel JSON API
export function server_url() {
  if (isDevMode()) {
    return 'https://localhost:2021/api/v1/';
  } else {
    // return 'http://localhost:7073/api/v1/';
    return 'https://server.victor247.com/api/v1/';
  }
}


// This is url to get data from white label panel JSON API
export function adminServer_url() {
  if (isDevMode()) {
    return 'https://localhost:2020/api/v1/';
  } else {
    // return 'http://localhost:7073/api/v1/';
    return 'https://server.victor247.com/api/v1/';
  }
}

// This is socekt url to get data from office panel SOCKET
export function socket_url() {
  if (isDevMode()) {
    return 'ws://localhost:2021';
  } else {
    //return 'ws://localhost:2051';
    return 'https://socket.victor247.com';
  }
}

// This is socekt url to get data from office panel SOCKET
export function client_socket_url() {
  if (isDevMode()) {
    return 'ws://localhost:2020';
  } else {
    //return 'ws://localhost:2050';
    return 'https://server.victor247.com';
  }
}


// This is socekt url to get data from office panel SOCKET
export function client_socket_redis() {
  return 'https://sslbhav.rdsconn.com';
}

export function stockUsernamePrefix() {
  return '01';
}

// This is socekt url to get data from office panel SOCKET
export function studio_url() {
  if (isDevMode()) {
    return 'https://casino.top99.ml';
  } else {
    //return 'https://casino.top99.ml';
    return 'https://smdc.premiumgames.xyz';
  }
}



// This is url to get data from office panel JSON API
export function office_url() {
  if (isDevMode()) {
    return 'https://localhost:2021/api/v1/';
  } else {
    // return 'http://localhost:2021/api/v1/';
    return 'https://socket.victor247.com/api/v1/';
  }
}

export function constantKey() {
  return 'bqwOlSSbg9VLtQuMp3mB7OAWQQwrvj6V';
}

export function socketPrefix() {
  return 'victor247com-';
}

export function socketPrefix1() {
  return 'victor247com';
}

export function stockApi() {
  return 'https://api.bettraderfee.xyz/';
  //return 'https://devapi.bazaarconn.xyz/';
}


// import server ip
export function import_url() {
  return 'https://games555.ml';
}
// production
export function production() {
  return false;
}


// project config
export const PROJECT_NAME = 'Bettrader';
export const PROJECT_LOGO_URL = 'assets/images/digi_logo.png';
export let EXPOSURE = 0;


