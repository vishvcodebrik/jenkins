import { Component, OnInit, Inject, Injectable } from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import * as io from 'socket.io-client';
import { Observable} from 'rxjs';


declare var $ : any;
@Injectable()

export class SocketServiceDotnetStock{
  private url =  'https://socket.bettraderfee.xyz/'; //https://devsocket.bazaarconn.xyz/

  private socket;

  constructor(@Inject(DOCUMENT) private document: any,
              private router: Router,
              private location: Location
              ) {
                this.socket = io(this.url, {
                  autoConnect: true,
                  reconnection: true,
                  reconnectionDelay: 500,
                  reconnectionDelayMax: 1000,
                  reconnectionAttempts: Infinity,
                  transports: ['websocket','flashsocket','htmlfile','xhr-polling','jsonp-polling','polling']
                          }
                );
                this.socket.on("connect", () => {
                  const transport = this.socket.io.engine.transport.name; // in most cases, "polling"

                  this.socket.io.engine.on("upgrade", () => {
                    const upgradedTransport = this.socket.io.engine.transport.name; // in most cases, "websocket"
                  });
                });
            }

  //Join room function
  joinRoom(roomId){
    this.socket.emit('join-room', roomId);
  }
  leaveRoom(roomId){
    this.socket.emit('leave-room', roomId);
  }
  placeOrder(data){
    this.socket.emit('place-order', data);
  }
  requestTrade(data){
    this.socket.emit('request-user-tradedata', data);
  }

  userMarketStatus(userId){
    this.socket.emit('request-user-position', userId);
  }

  userBalanceStatus(userId){
    this.socket.emit('request-user-balance', userId);
  }


  public receiveTradeData = () => {
    return Observable.create((observer) => {
      this.socket.on('receive-user-tradedata', (message) => {
        observer.next(message);
      });
    });
  }

  public receiveTradeDataLive = () => {
    return Observable.create((observer) => {
      this.socket.on('receive-order', (message) => {
        observer.next(message);
      });
    });
  }

  public receiveUserMarketStatus = () => {
    return Observable.create((observer) => {
      this.socket.on('receive-user-market-status', (message) => {
        observer.next(message);
      });
    });
  }

  public receiveUserBalanceStatus = () => {
    return Observable.create((observer) => {
      this.socket.on('receive-user-balance', (message) => {
        observer.next(message);
      });
    });
  }




  disconnect(){
    this.socket.disconnect(0);
  }

  connect(){
    this.socket.connect(0);
  }

}
