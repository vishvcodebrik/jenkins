import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from './auth.service';
import {UtilityService} from '../globals/utilityService';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private _authService: AuthService,
              private _router: Router,
  ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authService.isAuthenticated()) {
    }

    this._router.navigate(['/login']);
    return false;
  }
}
