import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';
import {isUndefined} from 'util';
import * as env from '../globals/env';
import {UtilityService} from '../globals/utilityService';
import { map } from 'rxjs-compat/operator/map';

@Injectable({
  providedIn: 'root'
})
export class CasinoAuthService {
  adminServer_url: any = env.adminServer_url();
  office_url: any = env.office_url();
  

  constructor(private http: HttpClient, private utilityService: UtilityService
              ) { }


  checkToken(token): Observable<any> {
    return this.http.post(this.office_url + 'auth/check-user-auth',token)
      .pipe(tap(_ => this.log(`get images successfully`)));
  }

  log(message) {
    // console.log(message);
  }


}
