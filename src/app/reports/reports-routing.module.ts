import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { AccountStatementComponent } from './account-statement/account-statement.component';
import { ProfitLossComponent } from './profit-loss/profit-loss.component';
import {AuthGuard} from "../auth-gaurd/auth-guard.service";
import {OpenBetComponent} from './open-bet/open-bet.component';
import { ChipsSettingComponent } from './chips-setting/chips-setting.component';
import {PasswordResetComponent} from './password-reset/password-reset.component';
import {ExposerDetailsComponent} from './exposer-details/exposer-details.component';
import {RulesSettingComponent} from './rules-setting/rules-setting.component';
import {StockOrderDetailComponent} from './stock-order-detail/stock-order-detail.component';
import { StockAccountSummeryComponent } from './stock-account-summery/stock-account-summery.component';
import { StockMarketReportComponent } from './stock-market-report/stock-market-report.component';
import { StockMarketDealsComponent } from './stock-market-deals/stock-market-deals.component';
import { StockCashSettlementComponent } from './stock-cash-settlement/stock-cash-settlement.component';
import { GenerateBillComponent } from './generate-bill/generate-bill.component';
import { HistoricalAcStatementComponent } from './historical-ac-statement/historical-ac-statement.component';


const routes: Routes = [{
  path: 'reports',
  component: ReportsComponent,
  children: [
    {
      path: 'as-view',
      canActivate: [AuthGuard],
      component: AccountStatementComponent,
    },
    {
      path: 'historical-ac-statement',
      canActivate: [AuthGuard],
      component: HistoricalAcStatementComponent,
    },
    {
      path: 'pl-view',
      canActivate: [AuthGuard],
      component: ProfitLossComponent,
    },
    {
      path: 'bt-view',
      canActivate: [AuthGuard],
      component: OpenBetComponent,
    },{
      path: 'chips-setting',
      canActivate: [AuthGuard],
      component: ChipsSettingComponent,
    },{
      path: 'password-reset',
      canActivate: [AuthGuard],
      component: PasswordResetComponent,
    },
    {
      path: 'rules-setting',
      canActivate: [AuthGuard],
      component: RulesSettingComponent,
    },{
      path: 'exposer-details',
      canActivate: [AuthGuard],
      component: ExposerDetailsComponent,
    },{
      path: 'stock-order-details',
      canActivate: [AuthGuard],
      component: StockOrderDetailComponent,
    },{
      path: 'stock-summary',
      canActivate: [AuthGuard],
      component: StockAccountSummeryComponent,
    },{
      path: 'stock-report',
      canActivate: [AuthGuard],
      component: StockMarketReportComponent,
    },{
      path: 'trade-list',
      canActivate: [AuthGuard],
      component: StockMarketDealsComponent,
    },
    {
      path: 'stock-settlement',
      canActivate: [AuthGuard],
      component: StockCashSettlementComponent,
    },{
      path: 'create-bill',
      canActivate: [AuthGuard],
      component: GenerateBillComponent,
    },
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
