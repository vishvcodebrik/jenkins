import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalAcStatementComponent } from './historical-ac-statement.component';

describe('HistoricalAcStatementComponent', () => {
  let component: HistoricalAcStatementComponent;
  let fixture: ComponentFixture<HistoricalAcStatementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalAcStatementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalAcStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
