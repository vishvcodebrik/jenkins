import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChipsSettingComponent } from './chips-setting.component';

describe('ChipsSettingComponent', () => {
  let component: ChipsSettingComponent;
  let fixture: ComponentFixture<ChipsSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChipsSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChipsSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
