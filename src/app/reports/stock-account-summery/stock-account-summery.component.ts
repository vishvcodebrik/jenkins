import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { UserService } from "../../services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { DataTableDirective } from 'angular-datatables';
import { Subject } from "rxjs/Rx";
import * as env from '../../globals/env';
import { SocketServiceDotnetStock } from '../../globals/socketServiceDotnetStock';
import { NgbTimepickerConfig, NgbTimeStruct, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from "moment";
import {BinaryService} from '../../services/binary.service';
import {UtilityService} from '../../globals/utilityService';
import {formatDate} from '@angular/common';

declare let $: any;
declare let _: any;


@Component({
  selector: 'app-stock-account-summery',
  templateUrl: './stock-account-summery.component.html',
  styleUrls: ['./stock-account-summery.component.scss']
})
export class StockAccountSummeryComponent implements OnInit {

  betObjFilter = {
    user: '',
    weekOption: 'thisweek',
    from: '',
    to: '',
    plcheck : '',
    brcheck : ''
  };
  userFilter = {
    page: 1,
    limit: 5,
    search: null,
    id: null,
  };
  minDate: any;
  exchangeList: any;
  clientList: any;
  addDefaultData: any;
  scriptList: any;
  selectedValue = 'ALL';
  fromDate: any;
  toDate: any;
  tempFromDate : any;
  temptoDate: any;
  selectedUserItems:any;
  @Input() dateChanged: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  server_url: any = env.adminServer_url();
  userId = this.utilityService.returnLocalStorageData('userId');
  stockMarketId = this.utilityService.returnLocalStorageData('stockUserId');
  disabled:boolean = true;
  usernameError : boolean = false;
  adminServer_url: any = env.adminServer_url();
  customeWeek = false;
  clientListFilter : any;
  endSubmit = false;

  constructor(private userService: UserService, private binaryService: BinaryService, private http: HttpClient, private spinner: NgxSpinnerService, private utilityService: UtilityService,
              private socketServiceDotnetStock : SocketServiceDotnetStock,config: NgbTimepickerConfig) {
  }

  filter = {
    userId: this.userId,
    page: 1,
    limit: 10,
    startDate: null,
    endDate: null,
    match: '',
    sport: '',
    types: '',
    status: 'open',

  };
  allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'))
  transactionResponse: any;
  accountSummary : any;
  types: any;
  sDate:any;
  eDate:any;
  ngOnInit(): void {
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(7,'d').format("YYYY-MM-DD");

    this.betObjFilter.from = endDate;
    this.betObjFilter.to = pre;

    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(1,'d').format("DD/MM/YYYY");
    this.sDate = ePreShow;
    this.eDate = ePreShow;

    if (this.filter.userId === this.userId) {
      this.filter['types'] = 'own';
      this.filter.userId = this.userId;
    } else {
      this.filter['types'] = 'client';
    }
    this.getAllClient();
    this.getExchnageList();
    this.instrumentList();

    this.socketServiceDotnetStock
      .receiveTradeData()
      .subscribe((response) => {
        if(response){
          this.transactionResponse = response;
        }
      });
  }



  /**
   * @author TR
   * @date 23-06-2020
   * GetAll AcSummaryReport
   * @param data
   */


  searchFilterData() {
    if(this.endSubmit) {
        return;
    }
    this.endSubmit = true;
    this.betObjFilter.user =  this.stockMarketId;
    let fromDate;
    let toDate;
    
    if(this.betObjFilter.weekOption == "thisweek"){
      let weekData = this.utilityService.getCurrentWeekSS();
      fromDate = weekData.fromDate;
      toDate = weekData.toDate;
    }else if(this.betObjFilter.weekOption == "previousweek"){
      let weekData = this.utilityService.getPreviousWeekDateSS();
      fromDate = weekData.fromDate;
      toDate = weekData.toDate;
    }else{
      if(this.tempFromDate){
        this.fromDate =  this.tempFromDate;
      }
      if(this.temptoDate){
        this.toDate =  this.temptoDate;
      }
      if(this.fromDate && this.fromDate != ''){
        this.tempFromDate =  this.fromDate;
        fromDate = this.fromDate.year + "/" + this.fromDate.month + "/" + this.fromDate.day;
        fromDate = formatDate(fromDate,'dd-MMM-yyyy', 'en')
      }else{
        fromDate = this.betObjFilter.from;
        fromDate = formatDate(fromDate,'dd-MMM-yyyy', 'en')
      }

      if(this.toDate && this.toDate != ''){
        this.temptoDate =  this.toDate;
        toDate = this.toDate.year + "/" + this.toDate.month + "/" + this.toDate.day;
        toDate = formatDate(toDate,'dd-MMM-yyyy', 'en')
      }else{
        toDate = this.betObjFilter.to;
        toDate = formatDate(toDate,'dd-MMM-yyyy', 'en')
      }
    }
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
        this.utilityService.storeRefreshToken(refreshToken);
        this.endSubmit = false;
        let obj = {
          UserId: this.betObjFilter.user,
          LoginUserId : this.stockMarketId,
          fromDate : fromDate,
          toDate :  toDate,
          IsPL : (this.betObjFilter.plcheck)?'Y' : '',
          IsBr : (this.betObjFilter.brcheck)?'Y' : '',
          IsCash : '',
          PageRow : 1000,
          PageNo : 1,
          accessToken : responseToken.data.access_token
        }
        if(this.betObjFilter.from === null) {
          delete this.betObjFilter.from;
        }
        if(this.betObjFilter.to === null) {
          delete this.betObjFilter.to;
        }
        this.binaryService.getAccountStatementSummary(obj).subscribe(userList => {
          if(userList.result == 1){
            this.accountSummary = userList.detail;
          }
        });
      }else{
        this.utilityService.userLogout();
      }
    });
  }

  clearFilter() {
    this.betObjFilter = {
      user: '',
      weekOption: 'thisweek',
      from: '',
      to: '',
      plcheck : '',
      brcheck : ''
    };
    this.selectedUserItems = '';
    this.accountSummary = [];
  }

  /**
   * @author subhash
   * @date 20-08-2019
   * date bulding
   */
  chnageWeek(e){
    if(e == "custome"){
      this.customeWeek = true;
    }else{
      this.customeWeek = false;
    }
    this.betObjFilter.weekOption = e;
  }
  onDateSelect(e) {
    if (e.year) {
      this.toDate = null;
      this.disabled = false;
      this.minDate = {
        year: e.year,
        month: e.month ,
        day: e.day + 1
      };
      var dt = new Date();

      let year = dt.getFullYear();
      let month = ('0' + ((dt.getMonth()) + 1)).slice(-2);
      let date = ('0' + (dt.getDate() - 7)).slice(-2);
      if (('0' + ((dt.getMonth() - 1) + 1)).slice(-2) === '00') {
        year = year - 1;
        month = '12'
      }
      let endDate = dt.getFullYear() + "-" + ('0' + (dt.getMonth() + 1)).slice(-2) + "-" + ('0' + (dt.getDate())).slice(-2);

      this.eDate = ('0' + (dt.getDate())).slice(-2)+ "/" + ('0' + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear();
    } else {
      this.disabled = true;
    }
  }

  getAllTransactionForMarket(){
    let weekData = this.utilityService.getCurrentWeek();
    let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
    let fromDate = (this.fromDate)?formatDate(this.fromDate,'dd-MMM-yyyy', 'en'):weekData.toDate;
    let toDate = (this.toDate)?formatDate(this.toDate,'dd-MMM-yyyy', 'en'):weekData.toDate;
    console.log("fromDate++++++++++",fromDate);
    console.log("toDate++++++++++",toDate);
    this.socketServiceDotnetStock.joinRoom("mobile-user-room-" + userId); // socket join room

    let data = {
      LoginUserId : userId,
      RoomId : "mobile-user-room-" + userId,
      FromDate : fromDate,
      ToDate : toDate,
      OrderStatus : "S,P",
      PageNumber : 1,
      PageSize : 50,
      UserId: "",
      ExchangeId: "",
      ScriptId: ""
    }
    this.socketServiceDotnetStock.requestTrade(data); // socket emit
    this.socketServiceDotnetStock.userMarketStatus(userId); // socket emit
  }



  getAllClient(){
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
        this.utilityService.storeRefreshToken(refreshToken);
        let obj = {
          UserId: Number(this.utilityService.returnLocalStorageData('stockUserId')),
          accessToken : responseToken.data.access_token
        }
        this.binaryService.userList(obj).subscribe(userList => {
          if(userList.result == 1){
            this.clientList = userList.lstUserList;
          }
        });
      }else{
        this.utilityService.userLogout();
      }
    });
  }

  getExchnageList(){

    let myExchRes = this.utilityService.returnLocalStorageData('myExchRes');
    this.exchangeList = JSON.parse(myExchRes);
    console.log(this.exchangeList);
  }


  instrumentList() {
    this.addDefaultData = this.allScriptDataAry
    console.log(this.addDefaultData);
    this.spinner.hide();
  }



  clearDate(e){
    if(e.target.value === ''){
      this.getAllTransactionForMarket();
    }
  }

  download(){
    console.log("download");
  }

}
