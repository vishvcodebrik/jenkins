import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAccountSummeryComponent } from './stock-account-summery.component';

describe('StockAccountSummeryComponent', () => {
  let component: StockAccountSummeryComponent;
  let fixture: ComponentFixture<StockAccountSummeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAccountSummeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAccountSummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
