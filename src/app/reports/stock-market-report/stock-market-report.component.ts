import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { UserService } from "../../services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { ModalDirective } from "ngx-bootstrap";
import { DataTableDirective } from 'angular-datatables';
import * as env from '../../globals/env';
import { SocketServiceDotnetStock } from '../../globals/socketServiceDotnetStock';
import { NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import * as moment from "moment";
import {BinaryService} from '../../services/binary.service';
import {UtilityService} from '../../globals/utilityService';
import {formatDate} from '@angular/common';
declare let $: any;
declare let _: any;

@Component({
  selector: 'app-stock-market-report',
  templateUrl: './stock-market-report.component.html',
  styleUrls: ['./stock-market-report.component.scss']
})
export class StockMarketReportComponent implements OnInit {
  betObjFilter = {
    user: '',
    exchangeName: '',
    scriptName: '',
    from: '',
    to: '',
    status : 'S,P'
  };
  minDate: any;
  exchangeList: any;
  clientList: any;
  addDefaultData: any;
  filterdScript :any;
  fromDate: any;
  toDate: any;
  tempFromDate : any;
  temptoDate: any;
  @Input() dateChanged: any;
  @ViewChild('transactionModel', { static: false }) transactionModel: ModalDirective;
  @ViewChild('placebetModel', { static: false }) placebetModel: ModalDirective;
  @ViewChild('deleteTransaction', {static: false}) deleteTransaction: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  server_url: any = env.adminServer_url();
  userId = this.utilityService.returnLocalStorageData('userId');
  disabled:boolean = true;
  adminServer_url: any = env.adminServer_url();
  constructor(private userService: UserService, private binaryService: BinaryService, private http: HttpClient, private spinner: NgxSpinnerService, private utilityService: UtilityService,
              private socketServiceDotnetStock : SocketServiceDotnetStock,config: NgbTimepickerConfig) {
  }

  filter = {
    userId: this.userId,
    page: 1,
    limit: 10,
    startDate: null,
    endDate: null,
    match: '',
    sport: '',
    types: '',
    status: 'open',

  };
  localStorageData = JSON.parse(this.utilityService.returnLocalStorageData('userData'));
  allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'))
  transactionResponse: any;

  types: any;
  sDate:any;
  eDate:any;
  selectedUserItems:any;


  ngOnInit(): void {
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(7,'d').format("YYYY-MM-DD");

    this.betObjFilter.from = endDate;
    this.betObjFilter.to = pre;


    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(1,'d').format("DD/MM/YYYY");
    console.log(ePreShow);
    this.sDate = ePreShow;
    this.eDate = ePreShow;


    if (this.filter.userId === this.userId) {
      this.filter['types'] = 'own';
      this.filter.userId = this.userId;
    } else {
      this.filter['types'] = 'client';
    }
    this.getAllTransactionForMarket();
    this.getAllClient();
    this.getExchnageList();
    this.instrumentList();

    this.socketServiceDotnetStock
      .receiveTradeData()
      .subscribe((response) => {
        if(response){
          this.transactionResponse = response;
        }
      });
  }



  /**
   * @author TR
   * @date 23-06-2020
   * GetAll AcSummaryReport
   * @param data
   */


  searchFilterData() {   
    if(this.tempFromDate){
      this.fromDate =  this.tempFromDate;
    }
    if(this.temptoDate){
      this.toDate =  this.temptoDate;
    }
    if(this.fromDate && this.fromDate != ''){
      this.tempFromDate =  this.fromDate;
      this.fromDate = this.fromDate.year + "/" + this.fromDate.month + "/" + this.fromDate.day;
    }
    if(this.toDate && this.toDate != ''){
      this.temptoDate =  this.toDate;
      this.toDate = this.toDate.year + "/" + this.toDate.month + "/" + this.toDate.day;
    }

    if(this.betObjFilter.from === null) {
      delete this.betObjFilter.from;
    }
    if(this.betObjFilter.to === null) {
      delete this.betObjFilter.to;
    }
    console.log("this.fromDate+++++++++++++++++",this.fromDate);
    console.log("this.toDate+++++++++++++++++",this.toDate);
    this.getAllTransactionForMarket();
  }

  /**
   * @author subhash
   * @date 20-08-2019
   * date bulding
   */

  onDateSelect(e) {
    if (e.year) {
      this.toDate = null;
      this.disabled = false;
      this.minDate = {
        year: e.year,
        month: e.month ,
        day: e.day + 1
      };
      var dt = new Date();

      let year = dt.getFullYear();
      let month = ('0' + ((dt.getMonth()) + 1)).slice(-2);
      let date = ('0' + (dt.getDate() - 7)).slice(-2);
      if (('0' + ((dt.getMonth() - 1) + 1)).slice(-2) === '00') {
        year = year - 1;
        month = '12'
      }
      let endDate = dt.getFullYear() + "-" + ('0' + (dt.getMonth() + 1)).slice(-2) + "-" + ('0' + (dt.getDate())).slice(-2);
    } else {
      this.disabled = true;
    }
  }




  getAllTransactionForMarket(){
    let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
    let today = new Date();

    let fromDate = (this.fromDate)?formatDate(this.fromDate,'dd-MMM-yyyy', 'en'):formatDate(today,'dd-MMM-yyyy', 'en');
    let toDate = (this.toDate)?formatDate(this.toDate,'dd-MMM-yyyy', 'en'):formatDate(today,'dd-MMM-yyyy', 'en');
    console.log(moment(fromDate).format('DD/MM/YYYY'));
    console.log(moment(toDate).format('DD/MM/YYYY'));
    this.sDate = moment(fromDate).format('DD/MM/YYYY');
    this.eDate = moment(toDate).format('DD/MM/YYYY');

    this.socketServiceDotnetStock.joinRoom("mobile-user-room-" + userId); // socket join room

    let data = {
      LoginUserId : userId,
      RoomId : "mobile-user-room-" + userId,
      FromDate : fromDate,
      ToDate : toDate,
      OrderStatus : this.betObjFilter.status,
      PageNumber : 1,
      PageSize : 500,
      UserId: userId,
      ExchangeId: this.betObjFilter.exchangeName,
      ScriptId: this.betObjFilter.scriptName
    }
    console.log(data);
    this.socketServiceDotnetStock.requestTrade(data); // socket emit
    this.socketServiceDotnetStock.userMarketStatus(userId); // socket emit
    console.log(this.fromDate)
    console.log(this.toDate);
  }


  getAllClient(){
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
        this.utilityService.storeRefreshToken(refreshToken);
        let obj = {
          UserId: Number(this.utilityService.returnLocalStorageData('stockUserId')),
          accessToken : responseToken.data.access_token
        }
        this.binaryService.userList(obj).subscribe(userList => {
          if(userList.result == 1){
            this.clientList = userList.lstUserList;
          }
        });
      }else{
        this.utilityService.userLogout();
      }
    });
  }

  getExchnageList(){

    let myExchRes = this.utilityService.returnLocalStorageData('myExchRes');
    this.exchangeList = JSON.parse(myExchRes);
    console.log(this.exchangeList);
  }


  instrumentList() {
    this.addDefaultData = this.allScriptDataAry
    console.log(this.addDefaultData);
    this.spinner.hide();
  }

  clearDate(e){
    if(e.target.value === ''){
      this.getAllTransactionForMarket();
    }
  }


  clearFilter(){
    this.spinner.show();
    this.sDate = '';
    this.eDate = '';
    this.filter.startDate = null;
    this.filter.endDate = null;
    this.fromDate =null;
    this.toDate =null;
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(3,'d').format("YYYY-MM-DD");

    this.filter.startDate = endDate;
    this.filter.endDate = pre;

    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(3,'d').format("DD/MM/YYYY");
    this.sDate = ePreShow;
    this.eDate = ePreShow;
    this.spinner.hide();
    this.betObjFilter.exchangeName = '';
    this.betObjFilter.scriptName = '';
    this.betObjFilter.user = '';
    this.betObjFilter.status = 'S,P';

    this.selectedUserItems = '';
    this.getAllTransactionForMarket();
  }
  download(){
    console.log("download method");
  }
  changeExch(data){
    this.filterdScript = _.filter(this.addDefaultData , function (rest) {
      if(rest.ExchangeId == data){
        return rest;
      }
    });
  }


}
