import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMarketReportComponent } from './stock-market-report.component';

describe('StockMarketReportComponent', () => {
  let component: StockMarketReportComponent;
  let fixture: ComponentFixture<StockMarketReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockMarketReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMarketReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
