import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenBetComponent } from './open-bet.component';

describe('OpenBetComponent', () => {
  let component: OpenBetComponent;
  let fixture: ComponentFixture<OpenBetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenBetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
