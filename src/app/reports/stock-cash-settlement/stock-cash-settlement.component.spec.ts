import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockCashSettlementComponent } from './stock-cash-settlement.component';

describe('StockCashSettlementComponent', () => {
  let component: StockCashSettlementComponent;
  let fixture: ComponentFixture<StockCashSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockCashSettlementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockCashSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
