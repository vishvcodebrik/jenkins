import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { UserService } from "../../services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { ModalDirective } from "ngx-bootstrap";
import { DataTableDirective } from 'angular-datatables';
import { Subject } from "rxjs/Rx";
import * as env from '../../globals/env';
import {ActivatedRoute, Router} from '@angular/router';

import { SocketServiceDotnetStock } from '../../globals/socketServiceDotnetStock';
import { NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import * as moment from "moment";
import {BinaryService} from '../../services/binary.service';
import {UtilityService} from '../../globals/utilityService';
import {formatDate} from '@angular/common';

declare let $: any;
declare let _: any;

@Component({
  selector: 'app-stock-cash-settlement',
  templateUrl: './stock-cash-settlement.component.html',
  styleUrls: ['./stock-cash-settlement.component.scss']
})
export class StockCashSettlementComponent implements OnInit {


  betObjFilter = {
    user: '',
    withOpening: "true",
    withSettlement: "true",
    from: '',
    to: '',
    status : 'S,P'
  };
  userFilter = {
    page: 1,
    limit: 5,
    search: null,
    id: null,
  };
  minDate: any;
  deleteStockData: any;
  fromDate: any;
  toDate: any;
  tempFromDate : any;
  temptoDate: any;
  debitUser = [];
  creditUser = [];
  creditTotal = 0;
  debitTotal = 0;
  filterParamId;

  @Input() dateChanged: any;
  @ViewChild('transactionModel', { static: false }) transactionModel: ModalDirective;
  @ViewChild('placebetModel', { static: false }) placebetModel: ModalDirective;
  @ViewChild('deleteTransaction', {static: false}) deleteTransaction: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  server_url: any = env.adminServer_url();
  userId = this.utilityService.returnLocalStorageData('userId');
  stockMarketId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
  disabled:boolean = true;
  adminServer_url: any = env.adminServer_url();
  constructor(private userService: UserService, private binaryService: BinaryService, private http: HttpClient, private spinner: NgxSpinnerService, private utilityService: UtilityService,
              private socketServiceDotnetStock : SocketServiceDotnetStock,config: NgbTimepickerConfig, private router: Router, private route: ActivatedRoute) {
  }

  filter = {
    userId: this.userId,
    page: 1,
    limit: 10,
    startDate: null,
    endDate: null,
    match: '',
    sport: '',
    types: '',
    status: 'open',

  };
  types: any;
  sDate:any;
  eDate:any;
  selectedUserItems:any;
  endSubmit = false; 
  ngOnInit(): void {
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(7,'d').format("YYYY-MM-DD");

    this.betObjFilter.from = endDate;
    this.betObjFilter.to = pre;


    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(1,'d').format("DD/MM/YYYY");
    this.sDate = ePreShow;
    this.eDate = ePreShow;


    if (this.filter.userId === this.userId) {
      this.filter['types'] = 'own';
      this.filter.userId = this.userId;
    } else {
      this.filter['types'] = 'client';
    }

    this.route.params.subscribe(params => {
      if(params.id !== undefined){
        this.filterParamId = params.id;
        this.getAllTransactionForMarket(params.id);
      }else {
        this.getAllTransactionForMarket(this.stockMarketId);
      }
    });
  }



  /**
   * @author TR
   * @date 23-06-2020
   * GetAll AcSummaryReport
   * @param data
   */


  searchFilterData() {
    if(this.tempFromDate){
      this.fromDate =  this.tempFromDate;
    }
    if(this.temptoDate){
      this.toDate =  this.temptoDate;
    }
    if(this.fromDate && this.fromDate != ''){
      this.tempFromDate =  this.fromDate;
      this.fromDate = this.fromDate.year + "/" + this.fromDate.month + "/" + this.fromDate.day;
    }
    if(this.toDate && this.toDate != ''){
      this.temptoDate =  this.toDate;
      this.toDate = this.toDate.year + "/" + this.toDate.month + "/" + this.toDate.day;
    }
    if(this.betObjFilter.from === null) {
      delete this.betObjFilter.from;
    }
    if(this.betObjFilter.to === null) {
      delete this.betObjFilter.to;
    }
    if(this.filterParamId){
      this.getAllTransactionForMarket(this.filterParamId);
    }else{
      this.getAllTransactionForMarket(this.stockMarketId);
    }
  }

  /**
   * @author subhash
   * @date 20-08-2019
   * date bulding
   */

  onDateSelect(e) {
    if (e.year) {
      this.toDate = null;
      this.disabled = false;
      this.minDate = {
        year: e.year,
        month: e.month ,
        day: e.day + 1
      };
      var dt = new Date();

      let year = dt.getFullYear();
      let month = ('0' + ((dt.getMonth()) + 1)).slice(-2);
      let date = ('0' + (dt.getDate() - 7)).slice(-2);
      if (('0' + ((dt.getMonth() - 1) + 1)).slice(-2) === '00') {
        year = year - 1;
        month = '12'
      }
      let endDate = dt.getFullYear() + "-" + ('0' + (dt.getMonth() + 1)).slice(-2) + "-" + ('0' + (dt.getDate())).slice(-2);

      // this.eDate = ('0' + (dt.getDate())).slice(-2)+ "/" + ('0' + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear();
    } else {
      this.disabled = true;
    }
  }



  getAllTransactionForMarket(userId){
    let weekData = this.utilityService.getCurrentWeek();
    // let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
    let today = new Date();
    let fromDate = (this.fromDate)?formatDate(this.fromDate,'dd-MMM-yyyy', 'en'):formatDate(today,'dd-MMM-yyyy', 'en');
    let toDate = (this.toDate)?formatDate(this.toDate,'dd-MMM-yyyy', 'en'):formatDate(today,'dd-MMM-yyyy', 'en');
    console.log(moment(fromDate).format('DD/MM/YYYY'));
    console.log(moment(toDate).format('DD/MM/YYYY'));
    this.sDate = moment(fromDate).format('DD/MM/YYYY');
    this.eDate = moment(toDate).format('DD/MM/YYYY');


    let data = {
      LoginUserId : this.stockMarketId,
      FromDate : fromDate,
      ToDate : toDate,
      WithOpening : this.betObjFilter.withOpening,
      WithSettlement : this.betObjFilter.withSettlement,
      UserId: userId,
    }
    this.getCashSettlement(data);
  }

  getCashSettlement(obj){
    if(this.endSubmit) {
      return;
    }
    this.endSubmit = true;
    this.creditUser = [];
    this.debitUser = [];

    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
        this.utilityService.storeRefreshToken(refreshToken);
        this.endSubmit = false;
        obj.accessToken = responseToken.data.access_token;
        this.binaryService.getCashSettlement(obj).subscribe(userList => {
          if(userList.result == 1){
            let detail = userList.detail;
            let userCheckId = (this.filterParamId)?this.filterParamId:this.stockMarketId;
            detail.map((ite, index) =>{
              ite.downuser = (userCheckId < ite.UserId) ? true : false;
              if(ite.Balance > 0){
                this.creditTotal = ite.Balance + this.creditTotal;
                this.creditUser.push(ite);
              }else{
                this.debitTotal = ite.Balance + this.debitTotal;
                this.debitUser.push(ite);
              }
            });
          }
        }, error => {
          console.error("data not found");
        });
      }else{
        this.utilityService.userLogout();
      }
    });
  }


  clearDate(e){
    if(e.target.value === ''){
      if(this.filterParamId){
        this.getAllTransactionForMarket(this.filterParamId);
      }else{
        this.getAllTransactionForMarket(this.stockMarketId);
      }
    }
  }


  clearFilter(){
    this.spinner.show();
    this.sDate = '';
    this.eDate = '';
    this.filter.startDate = null;
    this.filter.endDate = null;
    this.fromDate =null;
    this.toDate =null;
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(3,'d').format("YYYY-MM-DD");

    this.filter.startDate = endDate;
    this.filter.endDate = pre;

    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(3,'d').format("DD/MM/YYYY");
    this.sDate = ePreShow;
    this.eDate = ePreShow;
    this.spinner.hide();
    this.betObjFilter.withOpening = "true";
    this.betObjFilter.withSettlement = "true";
    this.betObjFilter.user = '';
    this.betObjFilter.status = 'S,P';

    this.selectedUserItems = '';
    if(this.filterParamId){
      this.getAllTransactionForMarket(this.filterParamId);
    }else{
      this.getAllTransactionForMarket(this.stockMarketId);
    }
  }
  download(){
    console.log("download method");
  }



}
