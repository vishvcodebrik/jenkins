import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { UserService } from "../../services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { ModalDirective } from "ngx-bootstrap";
import { DataTableDirective } from 'angular-datatables';
import { Subject } from "rxjs/Rx";
import * as env from '../../globals/env';
import { MarketService } from '../../services/market.service';
import { SocketServiceDotnetStock } from '../../globals/socketServiceDotnetStock';
import { NgbTimepickerConfig, NgbTimeStruct, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from "moment";
import {BinaryService} from '../../services/binary.service';
import {UtilityService} from '../../globals/utilityService';
import {formatDate} from '@angular/common';

declare let $: any;
declare let _: any;


class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}
@Component({
  selector: 'app-stock-order-detail',
  templateUrl: './stock-order-detail.component.html',
  styleUrls: ['./stock-order-detail.component.scss']
})
export class StockOrderDetailComponent implements OnInit {

  betObj = {
    orderType: 'MARKET',
    qty: 1,
    user: '',
    exchangeName: '',
    scriptName: '',
    type: '',
    price: ''
  };
  betObjFilter = {
    user: '',
    exchangeName: '',
    scriptName: '',
    from: '',
    to: ''
  };
  minDate: any;
  deleteStockData: any;
  exchangeList: any;
  clientList: any;
  searchCtrl: any;
  exchSearchCtrl: any;
  scriptsearchCtrl: any;
  addDefaultData: any;
  scriptList: any;
  selectedValue = 'ALL';
  fromDate: any;
  toDate: any;
  @Input() dateChanged: any;
  @ViewChild('transactionModel', { static: false }) transactionModel: ModalDirective;
  @ViewChild('placebetModel', { static: false }) placebetModel: ModalDirective;
  @ViewChild('deleteTransaction', {static: false}) deleteTransaction: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  server_url: any = env.adminServer_url();
  dtTrigger = new Subject();
  resData: any;
  userId = this.utilityService.returnLocalStorageData('userId');
  allSport: any;
  allMatch: any;
  allClient: any;
  disabled:boolean = true;
  adminServer_url: any = env.adminServer_url();
  constructor(private userService: UserService, private binaryService: BinaryService, private http: HttpClient, private spinner: NgxSpinnerService, private utilityService: UtilityService,
    private socketServiceDotnetStock : SocketServiceDotnetStock,config: NgbTimepickerConfig) {
  }

  filter = {
    userId: this.userId,
    page: 1,
    limit: 10,
    startDate: null,
    endDate: null,
    match: '',
    sport: '',
    types: '',
    status: 'open',

  };
   localStorageData = JSON.parse(this.utilityService.returnLocalStorageData('userData'));
   allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'))
  transactionResponse: any;
  userTransactionRes: any;
  types: any;
  sDate:any;
  eDate:any;
  ngOnInit(): void {
    let pre =  moment().format("YYYY-MM-DD");
    let endDate =  moment().subtract(7,'d').format("YYYY-MM-DD");

    this.betObjFilter.from = endDate;
    this.betObjFilter.to = pre;

    let ePreShow = moment().format("DD/MM/YYYY");
    let sPreShow = moment().subtract(7,'d').format("DD/MM/YYYY");
    this.sDate = ePreShow;
    this.eDate = ePreShow;

    if (this.filter.userId === this.userId) {
      this.filter['types'] = 'own';
      this.filter.userId = this.userId;
    } else {
      this.filter['types'] = 'client';
    }
    // this.getAcSummaryReport();
    // this.getAllSportList();
    // this.getAllDownTreeUser();
    this.getAllTransactionForMarket();
    this.getAllClient();
    this.getExchnageList();
    this.instrumentList();

    this.socketServiceDotnetStock
      .receiveTradeData()
      .subscribe((response) => {
        if(response){
          console.log(response);
          this.transactionResponse = response;
        }
      });
    }



  /**
   * @author TR
   * @date 23-06-2020
   * GetAll AcSummaryReport
   * @param data
   */
  

  searchFilterData() {
    if(this.fromDate && this.fromDate != ''){
      this.fromDate = this.fromDate.year + "/" + this.fromDate.month + "/" + this.fromDate.day; 
    }
    if(this.toDate && this.toDate != ''){
      this.toDate = this.toDate.year + "/" + this.toDate.month + "/" + this.toDate.day; 
    }
     this.betObjFilter.user === '' ? delete this.betObjFilter.user : this.betObjFilter.user;
    this.betObjFilter.exchangeName === '' ? delete this.betObjFilter.exchangeName : this.betObjFilter.exchangeName;
    this.betObjFilter.scriptName === '' ? delete this.betObjFilter.scriptName : this.betObjFilter.scriptName;
    console.log(this.betObjFilter);
    if(this.betObjFilter.from === null) {
      delete this.betObjFilter.from;
    }
    if(this.betObjFilter.to === null) {
      delete this.betObjFilter.to;
    }
 
    this.getAllTransactionForMarket();
  }

  clearFilter() {
    this.betObjFilter = {
      user: '',
      exchangeName: '',
      scriptName: '',
      from: '',
      to: ''
    };
  }
  getAllDownTreeUser() {
    // const item = {
    //   id: this.utilityService.returnLocalStorageData('userId'),
    //   type: 'CLIENT',
    // };
    // this.userService.getAllDowntreeUser(item).subscribe(response => {
    //   this.allClient = response.data;
    // });
  }

  /**
   * @author subhash
   * @date 20-08-2019
   * date bulding
   */

  onDateSelect(e) {
    if (e.year) {
      this.toDate = null;
      this.disabled = false;
      this.minDate = {
        year: e.year,
        month: e.month ,
        day: e.day + 1
      };
      var dt = new Date();

      let year = dt.getFullYear();
      let month = ('0' + ((dt.getMonth()) + 1)).slice(-2);
      let date = ('0' + (dt.getDate() - 7)).slice(-2);
      if (('0' + ((dt.getMonth() - 1) + 1)).slice(-2) === '00') {
        year = year - 1;
        month = '12'
      }
      let endDate = dt.getFullYear() + "-" + ('0' + (dt.getMonth() + 1)).slice(-2) + "-" + ('0' + (dt.getDate())).slice(-2);

      this.eDate = ('0' + (dt.getDate())).slice(-2)+ "/" + ('0' + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear();
    } else {
      this.disabled = true;
    }
  }

  placeBetOpenModal(){
    this.placebetModel.show();
  }
  closebetModel(type){
    if(type === 'betmodal'){
      this.placebetModel.hide();
    }
    if(type === 'deleteModal'){
      this.deleteTransaction.hide();
    }

  }
  openTransModal(data){
    this.deleteStockData = data;
    this.deleteTransaction.show();
  }

  deleteTransactionStock(){
    this.binaryService.deleteTransactionById(this.deleteStockData).subscribe(response => {
      if(response){
        this.spinner.hide();
        this.getAllTransactionForMarket();
        this.deleteTransaction.hide();
        this.utilityService.popToast('success','Success', 3000 , 'delete successfully.');
      }
    }, error => {
      this.spinner.hide();
      console.error('error in get users settings');
    });
  }


  rerender(): void {
    $('.dataTable').dataTable().fnClearTable();
  }
  getAllTransactionForMarket(){    
    let weekData = this.utilityService.getCurrentWeek();
    let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
    let fromDate = (this.fromDate)?formatDate(this.fromDate,'dd-MMM-yyyy', 'en'):weekData.toDate;
    let toDate = (this.toDate)?formatDate(this.toDate,'dd-MMM-yyyy', 'en'):weekData.toDate;
    console.log("fromDate++++++++++",fromDate);
    console.log("toDate++++++++++",toDate);
    this.socketServiceDotnetStock.joinRoom("mobile-user-room-" + userId); // socket join room
    
    let data = {
          LoginUserId : userId,
          RoomId : "mobile-user-room-" + userId,
          FromDate : fromDate,
          ToDate : toDate,
          OrderStatus : "S,P",
          PageNumber : 1,
          PageSize : 50,
          UserId: "",
          ExchangeId: "",
          ScriptId: ""
        }
        this.socketServiceDotnetStock.requestTrade(data); // socket emit
        this.socketServiceDotnetStock.userMarketStatus(userId); // socket emit
  }


  getAllClient(){
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){ 
        let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          let obj = {
            UserId: Number(this.utilityService.returnLocalStorageData('stockUserId')),
            accessToken : responseToken.data.access_token
          }
          this.binaryService.userList(obj).subscribe(userList => {
            if(userList.result == 1){
              this.clientList = userList.lstUserList;
            }
          });
      }
    });
    // this.binaryService.getAllClient().subscribe(response => {
    //   this.clientList = response.data;
    //   // this.utilityService.popToast('success','Success', 3000 , 'Delete default successfully.');
    //   this.spinner.hide();
    // }, error => {
    //    this.utilityService.popToast('error','Error', 3000 , error);
    // });
  }

  getExchnageList(){

    let myExchRes = this.utilityService.returnLocalStorageData('myExchRes');
    this.exchangeList = JSON.parse(myExchRes);
    console.log(this.exchangeList);
  }

  exchonSelectionChange(event){
   this.selectedValue = event.value;
  }

  scriptonSelectionChange(event){
    console.log(event)
    this.scriptList = _.filter(this.addDefaultData, function(e)
    {
      return e.name === event.value;
    });
  }

  instrumentList() {
    this.addDefaultData = this.allScriptDataAry
    console.log(this.addDefaultData);
    this.spinner.hide();
    // this.binaryService.getAllInstDefault(data).subscribe(response => {
    //   this.addDefaultData = response.data;
    //   console.log(this.addDefaultData);
    //   this.spinner.hide();
    // }, error => {
    //   console.error('error in get default');
    // });
  }

  placeOrder(){
    this.spinner.show();
    let data = {
      InstrumentId: this.scriptList[0]._id,
      InstrumentToken: this.scriptList[0].instrumentToken,
      // ipAddress: this.localStorageData.ipAddress,
      // masterIds: this.exchangeList[0].masterIds,
      orderPrice: this.betObj.price,
      orderType: "MARKET",
      // expiryDate: this.scriptList[0].expiry,
      parentId: this.exchangeList[0].parentId,
      quantity: this.betObj.qty,
      type: this.betObj.type ,
      userId: this.betObj.user,
      // createdBy: this.utilityService.returnLocalStorageData('userId')
    };

    // this.binaryService.placeOrderAdm(data).subscribe(response => {
    //   if(response.status){
    //     this.spinner.hide();
    //     this.placebetModel.hide();
    //      this.utilityService.popToast('success','Success', 3000 , 'Order place successfully.');
    //   } else {
    //     this.spinner.hide();
    //     this.placebetModel.hide();
    //     this.utilityService.popToast('error','Error', 3000 , response.message);
    //   }
    // }, error => {
    //   this.spinner.hide();
    //   this.placebetModel.hide();
    //   console.error('error in Order place');
    // });
  }

  clearDate(e){
    if(e.target.value === ''){
      this.getAllTransactionForMarket();
    }

  }

}
