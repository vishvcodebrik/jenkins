import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesSettingComponent } from './rules-setting.component';

describe('RulesSettingComponent', () => {
  let component: RulesSettingComponent;
  let fixture: ComponentFixture<RulesSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RulesSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
