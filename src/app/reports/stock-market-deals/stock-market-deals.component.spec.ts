import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMarketDealsComponent } from './stock-market-deals.component';

describe('StockMarketDealsComponent', () => {
  let component: StockMarketDealsComponent;
  let fixture: ComponentFixture<StockMarketDealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockMarketDealsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMarketDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
