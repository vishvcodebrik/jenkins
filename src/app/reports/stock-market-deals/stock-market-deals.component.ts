import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { UserService } from "../../services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpClient } from "@angular/common/http";
import { ModalDirective } from "ngx-bootstrap";
import { DataTableDirective } from 'angular-datatables';
import * as env from '../../globals/env';
import {BinaryService} from '../../services/binary.service';
import {UtilityService} from '../../globals/utilityService';
import {interval} from 'rxjs';
import {CommonService} from '../../services/common.service';
import {SocketServiceBbMarket} from '../../globals/socketServiceBbMarket';
import { SocketServiceDotnetStock } from '../../globals/socketServiceDotnetStock';

declare let $: any;
declare let _: any;


@Component({
  selector: 'app-stock-market-deals',
  templateUrl: './stock-market-deals.component.html',
  styleUrls: ['./stock-market-deals.component.scss']
})
export class StockMarketDealsComponent implements OnInit {
  instrumentTokenIds = [];
  minDate: any;
  exchangeList: any;
  clientList: any;
  addDefaultData: any;
  subscription;
  subscriptionInstrument;

  @ViewChild('transactionModel', { static: false }) transactionModel: ModalDirective;
  @ViewChild('placebetModel', { static: false }) placebetModel: ModalDirective;
  @ViewChild('deleteTransaction', {static: false}) deleteTransaction: ModalDirective;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  server_url: any = env.adminServer_url();
  userId = this.utilityService.returnLocalStorageData('userId');
  randomString = this.commonService.getRandomString(10);
  constructor(private userService: UserService, private binaryService: BinaryService, private http: HttpClient, private spinner: NgxSpinnerService, private utilityService: UtilityService, private commonService: CommonService, private socketServiceBbMarket : SocketServiceBbMarket, private socketServiceDotnetStock : SocketServiceDotnetStock) {}

  filter = {
    userId: this.userId,
    page: 1,
    limit: 10,
    match: '',
    sport: '',
    types: '',
    status: 'open',

  };
  localStorageData = JSON.parse(this.utilityService.returnLocalStorageData('userData'));
  transactionResponse: any;

  types: any;
  marginUsed;
  marketStatus;
  realisedPl;
  credit;
  allDeals:any;
  defaultScript: any = [];
  userLoginId:any;
  allScriptDataAry = JSON.parse(this.utilityService.returnLocalStorageData('allScript'))
  m2mreal;
  m2mTotal;
  equity;
  freeMargin;



  ngOnInit(): void {
    let users = this.commonService.getLocalStorage();
    this.userLoginId = users.userId;
    this.socketServiceBbMarket.joinRoom( this.userLoginId);

    if (this.filter.userId === this.userId) {
      this.filter['types'] = 'own';
      this.filter.userId = this.userId;
    } else {
      this.filter['types'] = 'client';
    }
    this.getAlldefaultScript();
    this.getAllClient();
    this.getExchnageList();
    this.instrumentList();
    let userId = Number(this.utilityService.returnLocalStorageData('stockUserId'))
    this.socketServiceDotnetStock.joinRoom("mobile-user-room-" + userId); // socket join room
    this.socketServiceDotnetStock.userMarketStatus(userId); // socket emit
    this.socketServiceDotnetStock.userBalanceStatus(userId); // socket emit


    this.socketServiceDotnetStock
      .receiveUserMarketStatus()
      .subscribe((response) => {
        console.log("response+++++++++++++",response);
        // this.marketStatus = response[0];
        // this.realisedPl =  (this.marketStatus[0].PL + this.marketStatus[0].BK).toFixed(2);
        // this.credit = (this.marketStatus[0].CRD).toFixed(2);
        // this.marginUsed = (this.marketStatus[0].MRGU).toFixed(2);
        let instrumentTokenAry = [];
        this.transactionResponse = _.filter(response , function (rest) {
          console.log(response);
          instrumentTokenAry.push(rest.ScriptToken);

          if(Number(rest.BuyQty) > 0){
            rest.type = "Buy";
            rest.class = 'credit-color';
            rest.price = rest.BuyPrice;
            rest.quentity = rest.BuyQty;
          }else{
            rest.type = "Sell";
            rest.class = 'debit-color';
            rest.price = rest.SellPrice;
            rest.quentity = rest.SellQty;
          }
          let openQty = Number(rest.BuyQty) - Number(rest.SellQty);
          let openPrice = Number(rest.BuyPrice) - Number(rest.SellPrice);
          let openAvgPrice = openPrice/openQty;
          rest.openAvgPrice = openAvgPrice.toFixed(2);
          rest.openQty = openQty; 
          rest.LotSize = Number(rest.LotSize);
          return rest;
        });
        let userId =  Number(this.utilityService.returnLocalStorageData('stockUserId'));
        let tokenIds =  instrumentTokenAry.toString();
        var requested = {
          "roomId": "instrumentSocket_" + userId,
          "instrument_token": tokenIds,
          "uniqId":this.commonService.getRandomString(10),
          "response_type" : 'full'
        };
        if(this.subscription){
          this.clearSubscription(this.subscriptionInstrument);
        }
        var observable = interval(600);
        this.subscriptionInstrument = observable.subscribe(x => {
          this.socketServiceBbMarket.setInstrument(JSON.stringify(requested));
        });
        this.getRate();
      });

      this.socketServiceDotnetStock
      .receiveUserBalanceStatus()
      .subscribe((response) => {
        this.marketStatus = response[0];
        console.log(response[0]['PL']);
        this.realisedPl = (this.marketStatus['PL'] + this.marketStatus['BK']).toFixed(2);
        console.log(this.realisedPl);
        this.credit = (this.marketStatus.CRD).toFixed(2);
        this.marginUsed = (this.marketStatus.MRGU).toFixed(2);
      });

  }


  getAlldefaultScript(){
    this.spinner.show();
    let data = {
      userId: this.utilityService.returnLocalStorageData('userId'),
    };
    let defultScript = this.utilityService.returnLocalStorageData('defultScript');
    this.defaultScript = JSON.parse(defultScript);


    let totalRec = this.defaultScript.length;
    this.defaultScript.map((ite, index) =>{
      if(ite !== null) {
        let findScriptId =  this.allScriptDataAry.find(obj => obj.ScriptToken == ite.ScriptToken);
        if(findScriptId){
          ite.ScriptId = findScriptId.ScriptId,
            ite.ExchangeId = findScriptId.ExchangeId
        }
        this.instrumentTokenIds.push(ite.ScriptToken);
        if(totalRec - 1 === index){
          this.callSocketRate(this.instrumentTokenIds);
          this.getRate();
        }
      }
    });
    this.spinner.hide();


  }


  getAllClient(){
    this.userService.getNewToken().subscribe(responseToken => {
      if(responseToken.status == true){
        let refreshToken = responseToken.data.refresh_token;
        this.utilityService.storeRefreshToken(refreshToken);
        let obj = {
          UserId: Number(this.utilityService.returnLocalStorageData('stockUserId')),
          accessToken : responseToken.data.access_token
        }
        this.binaryService.userList(obj).subscribe(userList => {
          if(userList.result == 1){
            this.clientList = userList.lstUserList;
          }
        });
      }
    });
  }
  getExchnageList(){

    let myExchRes = this.utilityService.returnLocalStorageData('myExchRes');
    this.exchangeList = JSON.parse(myExchRes);
    console.log(this.exchangeList);
  }


  instrumentList() {
    this.addDefaultData = this.allScriptDataAry
    console.log(this.addDefaultData);
  }

  callSocketRate(tokens){
    let uniqId = "";
    let tokenIds =  tokens.toString();
    //socket service market join
    var requested = {
      "roomId": this.randomString,
      "instrument_token": tokenIds,
      "uniqId":this.commonService.getRandomString(10),
      "response_type" : 'full'
    };
    if(this.subscription){
      this.clearSubscription(this.subscription);
    }
    var observable = interval(600);
    this.subscription = observable.subscribe(x => {
      this.socketServiceBbMarket.setInstrument(JSON.stringify(requested));
    });
  }
  clearSubscription(timer){
    let storeTimer = timer;
    setTimeout(function(){
    },3000);
  }

  getRate(){
    let userIdsd = this.utilityService.returnLocalStorageData('userId')
    this.socketServiceBbMarket
      .getInstrument()
      .subscribe((response) => {
        if(response.data.length > 0){
          response.data.map((result, index) =>{
            if(result){
              let data = result;
              if(this.transactionResponse){
                let findScriptId =  this.transactionResponse.find(obj => obj.ScriptToken == data.instrument_token);
                if(findScriptId){
                  let currentPrice = (findScriptId.openQty>0)?data['buy_price_0']:data['sell_price_0'];
                  let mtmpl = (currentPrice - Number(findScriptId.openAvgPrice)) * findScriptId.openQty * findScriptId.LotSize;
                  findScriptId.profileAndLoss = mtmpl;
                  // console.log("stock market deals+++++++++++++++",currentPrice);
                  // console.log("stock market findScriptId.openAvgPrice+++++++++",findScriptId.openAvgPrice);
                  // console.log("stock market findScriptId.openQty+++++++++",findScriptId.openQty);
                  // console.log("stock market findScriptId.LotSize+++++++++",findScriptId.LotSize);
                  // console.log("mtmpl++++++++++++",mtmpl);
                    $("#" + data.instrument_token + "_cmp").html(String(currentPrice.toFixed(2)));
                  $("#" + data.instrument_token + "_mmpl").html(String(mtmpl.toFixed(2)));
                  $("#" + data.instrument_token + "_cmp_mob").html(String(currentPrice.toFixed(2)));
                  $("#" + data.instrument_token + "_mmpl_mob").html(String(mtmpl.toFixed(2)));
                  this.updateTotal();
                }
              }
              $("#" + data.instrument_token + "_buy_price_0").html(data['buy_price_0']);
              $("#"+  data.instrument_token + '_buy_price_0_netChange').html(data['buy_price_0']);
              $("#"+  data.instrument_token + '_buy_price_0_netChange_mob').html(data['buy_price_0']);

              $("#"+  data.instrument_token + '_sell_price_0_netChange').html(data['sell_price_0']);
              $("#"+  data.instrument_token + '_sell_price_0_netChange_mob').html(data['sell_price_0']);

              let preAmntBuy = $("#prev_" + data.instrument_token + "_buy_price_0").val();
              if(data['buy_price_0'] > preAmntBuy){
                $("#" + data.instrument_token  + "_buy_price_0").removeClass('red').addClass('green');
                $("#" + data.instrument_token + "_buy_price_0_mob").removeClass('red').addClass('green');
              }else{
                $("#" + data.instrument_token  + "_buy_price_0").removeClass('green').addClass('red');
                $("#" + data.instrument_token + "_buy_price_0_mob").removeClass('green').addClass('red');
              }
              $("#prev_" + data.instrument_token + "_buy_price_0").val(data['buy_price_0']);


              let preAmntSell = $("#prev_" + data.instrument_token + "_sell_price_0").val();
              if(data['sell_price_0'] > preAmntSell){
                $("#" + data.instrument_token  + "_sell_price_0").removeClass('red').addClass('green');
                $("#" + data.instrument_token + "_sell_price_0_mob").removeClass('red').addClass('green');
              }else{
                $("#" + data.instrument_token  + "_sell_price_0").removeClass('green').addClass('red');
                $("#" + data.instrument_token + "_sell_price_0_mob").removeClass('green').addClass('red');
              }
              $("#prev_" + data.instrument_token + "_sell_price_0").val(data['sell_price_0']);

              $("#" + data.instrument_token + "_sell_price_0").html(data['sell_price_0']);
              $("#" + data.instrument_token + "_last_price").html(data['last_price']);
              $("#" + data.instrument_token + "_net_change").html(data['net_change']);
              $("#" + data.instrument_token + "_ohlc_high").html(data['ohlc_high']);
              $("#" + data.instrument_token + "_ohlc_low").html(data['ohlc_low']);
              $("#" + data.instrument_token + "_last_trade_time").html(data['last_trade_time']);

              $("#" + data.instrument_token + "_buy_price_0_mob").html(data['buy_price_0']);
              $("#" + data.instrument_token + "_sell_price_0_mob").html(data['sell_price_0']);
              $("#" + data.instrument_token + "_last_price_mob").html(data['last_price']);
              $("#" + data.instrument_token + "_net_change_mob").html(data['net_change']);
              $("#" + data.instrument_token + "_ohlc_high_mob").html(data['ohlc_high']);
              $("#" + data.instrument_token + "_ohlc_low_mob").html(data['ohlc_low']);
              $("#" + data.instrument_token + "_last_trade_time_mob").html(data['last_trade_time']);

              if(data['net_change'] >0){
                $("#"+data.instrument_token + ' ' + 'i').removeClass('fa fa-arrow-circle-down red');
                $("#"+data.instrument_token + ' ' + 'i').addClass('fa fa-arrow-circle-up green');
                $("#"+data.instrument_token + '_mob').removeClass('red1');
                $("#"+data.instrument_token + '_mob').addClass('green1');
                $("#"+data.instrument_token + '_net_change_mob').removeClass('red');
                $("#"+data.instrument_token + '_net_change_mob').addClass('green');
              }else{
                $("#"+data.instrument_token + ' ' + 'i').removeClass('fa fa-arrow-circle-up green');
                $("#"+data.instrument_token + ' ' + 'i').addClass('fa fa-arrow-circle-down red');
                $("#"+data.instrument_token + '_mob').removeClass('green1');
                $("#"+data.instrument_token + '_mob').addClass('red1');
                $("#"+data.instrument_token + '_net_change_mob').removeClass('green');
                $("#"+data.instrument_token + '_net_change_mob').addClass('red');
              }
            }
          });

        }
      });
  }


  updateTotal(){
    let sum = 0
    _.filter(this.allDeals, function(item) {
      if(item.profileAndLoss){
        sum = sum + item.profileAndLoss;
      }
    });
    this.m2mTotal = sum.toFixed(2);
    this.m2mreal = (Number(this.m2mTotal) + Number(this.realisedPl)).toFixed(2);
    this.equity =  (Number(this.m2mreal) + Number(this.credit)).toFixed(2);
    this.freeMargin = (Number(this.equity) - Number(this.marginUsed)).toFixed(2);
  }

  download(){
    console.log("download");
  }
}
