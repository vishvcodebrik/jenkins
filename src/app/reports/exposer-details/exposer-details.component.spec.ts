import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExposerDetailsComponent } from './exposer-details.component';

describe('ExposerDetailsComponent', () => {
  let component: ExposerDetailsComponent;
  let fixture: ComponentFixture<ExposerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExposerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExposerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
