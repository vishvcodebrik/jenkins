import {Component, OnInit, ViewChild} from '@angular/core';
import {MarketService} from "../services/market.service";
import {SocketService} from "../globals/socketService";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {ModalDirective} from 'ngx-bootstrap';
import {UserIdleService} from 'angular-user-idle';
import {UtilityService} from '../globals/utilityService';
import {SocketServiceClient} from '../globals/socketServiceClient';
import {UserService} from "../services/user.service";
import { CommonService } from "../services/common.service";
declare let _: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @ViewChild('childModal', { static: false }) childModal: ModalDirective;
  marketFilter = {
    page: 1,
    limit: 10,
    search: null,
    userId : this.utilityService.returnLocalStorageData('userId')
  };
  userId = this.utilityService.returnLocalStorageData('userId');
  userData = JSON.parse(this.utilityService.returnLocalStorageData('userData'));
  getAllMarkets :any;
  getAllMarketsSearch :any;
  searchDatas = [];
  counter: any;
  stockMarket: any;
  searchText: any;
  count = 0;
  getAll = [];
  isStockLogin : any;
  constructor(
    private marketService: MarketService ,
    private utilityService: UtilityService ,
    private userIdle: UserIdleService ,
    private  socketService: SocketService,
    private  socketServiceClient: SocketServiceClient ,
    private  spinner: NgxSpinnerService ,
    private commonService: CommonService,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit() {
    setTimeout(res =>{
      this.getMarketOdds();
      this.getMarketOddsItem();
    })

    // this.getLayoutSetting();
    this.socketService
      .changeFlag()
      .subscribe((response) => {
          if(response){
            setTimeout(res =>{
              this.getMarketOdds();
              this.getMarketOddsData();
            },2000)
          }
      });
      if (this.utilityService.returnLocalStorageData('stockLogin') == false) {
        setTimeout(() => {
          if(this.utilityService.returnLocalStorageData('stockLogin') == 'false'){
            this.isStockLogin = false;
          }else{
            this.isStockLogin = true;
          }
        },1000);
      }else{
        this.isStockLogin = true;
      }
    this.socketServiceClient
      .marketCreate()
      .subscribe((response) => {
          if(response){
           this.getMarketOdds();
            this.getMarketOddsData()
          }
      });

    this.socketServiceClient
      .updateSport()
      .subscribe((response) => {
        if(response){
          this.getMarketOdds();
          this.getMarketOddsData()
        }
      });

    this.socketServiceClient
      .updateTournament()
      .subscribe((response) => {
        if(response){
          this.getMarketOdds();
          this.getMarketOddsData()
        }
      });

    this.socketServiceClient
      .updateMatch()
      .subscribe((response) => {
        if(response){
          this.getMarketOdds();
          this.getMarketOddsData()

        }
      });

    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
        this.counter = count;
      this.utilityService.userLogout();
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigate(['']);
      setTimeout(function(){ window.location.reload(); }, 30);
      }
    );

    // // Start watch when time is up.
    // this.userIdle.onTimeout().subscribe(() =>{
    //   this.utilityService.userLogout();
    //   localStorage.clear();
    //   sessionStorage.clear();
    //   this.router.navigate(['']);
    //   setTimeout(function(){ window.location.reload(); }, 100);
    // });
  }

  /**
   * @author TR
   * @date : 26-02-2020
   * get all market
   */
  getMarketOdds() {
    let sidebar = localStorage.getItem("sidebarData");
    if(sidebar) {
      sidebar = JSON.parse(sidebar);
      this.getAllMarkets = sidebar;
      this.getAllMarketsSearch = sidebar;
    }
    let reply = localStorage.getItem("dashboardData")
      if(reply){
        reply = JSON.parse(reply);
        _.map(reply, function (e) {
          if(e._id == 4){
            localStorage.setItem('cricketData', JSON.stringify(e));
          } else if(e._id == 1){
            localStorage.setItem('tennisData', JSON.stringify(e));
          }else if(e._id == 2){
            localStorage.setItem('soccerData', JSON.stringify(e));
          }else{
            localStorage.setItem('otherSport', JSON.stringify(e));
          }
        })

      }

  }

  getMarketOddsItem() {
    let reply = localStorage.getItem("dashboardData")
      if(reply){
        reply = JSON.parse(reply);
        reply =    _.filter(reply, function (e) {
          e.doc = _.filter(e.doc, function (i) {
            // i.data = _.filter(i.data, function (p) {
              if(i.isPlay == true){
                return i;
              }
            // })
            // if(i.data.length > 0){
            //   return i;
            // }

          })
          if(e.doc.length > 0) {
            return e;
          }
        })

        let gfg = _.sortBy(reply, ['doc[0].sportUrl.displayOrder']);
        localStorage.setItem('inplayMatch',JSON.stringify(gfg));

      }
  }

  getMarketOddsData() {
    this.marketService.getAllMarketForSideCreative(this.marketFilter, this.userId).subscribe(response => {

      localStorage.setItem('sidebarData', JSON.stringify(response.data));

    });

  }

  /**
   * @author TR
   * @date : 26-02-2020
   * get all market
   */
  matchDataSearch() {
    this.marketService.getAllMarketForSidebar(this.marketFilter).subscribe(response => {
      response = this.utilityService.gsk(response.auth);
      response = JSON.parse(response);
      if(response){
        this.getAllMarkets = response.data;
        console.log(this.getAllMarkets);
      }
      // console.log(this.getAllMarkets);
      // this.getAllMarkets.data = this.getAllMarkets.data.map(data =>{
      //   data.doc = _.unique(data.doc, function (datas) {
      //       return datas.match.id;
      //     });
      //   return data;
      // })
    });
  }


  getLayoutSetting(){
    this.marketService.getLayoutSetting().subscribe(response => {
      if (response.status) {
        this.stockMarket = response.data.stockMarket;
      } else {
        // this.commonService.popToast('error','Error', 3000 , 'not found market rules.');
      }
    }, error => {
      this.commonService.popToast('error', 'Error', 3000, 'not found.');
    });
  }
  searchCustom(){

    let items= [];
    let items1= [];
    let src = this.searchText;
      if(src.length > 2){
          _.map(this.getAllMarketsSearch , function(e) {
        _.map(e.doc , function(s) {
          items = _.filter(s.data , function(ite) {
           return ite.name.toLowerCase().includes(src.toLowerCase());
          });
          if(items.length > 0){
            _.map(items , function (p) {
              items1.push(p);
            })

          }
        });
      });

    }
    this.searchDatas = items1;
      console.log(this.searchDatas)
  }
  selectMatch(id){
    this.searchText = '';
    $('.side-bar').removeClass('is-open');
    $('#srch').val('');
    this.searchDatas = [];
    this.router.navigateByUrl('/sports/game-view/' + id);
  }
  closeSideBar(){
    $('.side-bar').removeClass('is-open');
  }

}
