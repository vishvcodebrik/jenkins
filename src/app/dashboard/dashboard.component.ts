import {Component, OnDestroy, OnInit, ViewChild,AfterViewInit} from '@angular/core';
import {MarketService} from "../services/market.service";
import {CurrencyService} from '../services/currency.service';
import {SocketService} from "../globals/socketService";

import {NgxSpinnerService} from "ngx-spinner";
import {UtilityService} from '../globals/utilityService';
import {SocketServiceClient} from '../globals/socketServiceClient';
import {SocketServiceRedis} from '../globals/socketServiceRedis';
import {SocketServiceRedisMarket} from '../globals/socketServiceRedisMarket';
import {ModalDirective} from 'ngx-bootstrap';

import { CommonService } from '../services/common.service';
import _ from "lodash";
import * as $ from "jquery";
import {ActivatedRoute, Router} from '@angular/router';
import * as env from '../globals/env';
import {ScoreCard} from "../globals/scoreCard";
// import firebase from "firebase";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit ,OnDestroy {
  @ViewChild('otpModal', { static: false }) otpModal: ModalDirective;
  slideConfig = {"slidesToShow": 1, "slidesToScroll": 1, "autoplay": true,  "vertical": false , arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
            slidesToShow: 1
        }
      }
    ]
  };
  constructor(private marketService: MarketService,  private scoreCard: ScoreCard,   private route: ActivatedRoute , private router: Router,private commonService: CommonService, private utilityService: UtilityService , private spinner: NgxSpinnerService, private socketService: SocketService, private socketServiceRedis: SocketServiceRedis, private socketServiceClient: SocketServiceClient,private socketServiceRedisMarket : SocketServiceRedisMarket,private currencyService :CurrencyService) {
  }



  marketFilter = {
    page: 1,
    limit: 10,
    search: null,
    data: null
  };
  contentHide = false;
  bannerImages: any;
  getAllMarkets = [];
  getAllSports = [];
  paramTypes = null;
  userId = this.utilityService.returnLocalStorageData('userId');
  selectedUniqIndx: any;
  runners: any;
  casinoImages: any = [];
  Toast = {
    type: '',
    title: '',
    timeout: 0,
    body: ''
  };
  projectName: any = env.PROJECT_NAME;
  dummyRateResponse = localStorage.getItem('dummyRateResponse');

  ngOnInit() {
    this.getMarketOddsData();
    this.getUserCurrency();

    this.route.params.subscribe(params => {
     if(params.type){
      this.paramTypes = params.type;
       this.dashboardData(params.type);
     }else{
       this.paramTypes = null;
       setTimeout(res =>{
         this.dashboardData('inplay');
         this.getMarketOddsData();
       })
     }
      // this.paramId = params.id;
      // if(params.id !== undefined){
      // }else {
      //   this.paramId = null;
      // }
    });


    const oldMatch = JSON.parse(sessionStorage.getItem('matchOddsIds'));
    const oldfancy = JSON.parse(sessionStorage.getItem('lastJoinRoom'));
    let scoreIds =  localStorage.getItem('scorecardIds');
    this.scoreCard.leaveRoom(scoreIds);
    let socketFancy = this.socketServiceRedis;
    let socketMatchOdds = this.socketServiceRedisMarket;
    _.map(oldfancy, function (e) {
      socketFancy.leaveRoom(e);
    })
    _.map(oldMatch, function (s) {
      socketMatchOdds.leaveRoom(s);
    })


    this.socketServiceRedis
      .reconnections()
      .subscribe((response) => {
        this.getMarketOddsData();
        this.route.params.subscribe(params => {
          if(params.type){
            this.paramTypes = params.type;
            this.dashboardData(params.type);

          }else{
            this.paramTypes = null;
          }
        });
        const oldMatch = JSON.parse(sessionStorage.getItem('matchOddsIds'));
        const oldfancy = JSON.parse(sessionStorage.getItem('lastJoinRoom'));
        let scoreIds =  localStorage.getItem('scorecardIds');
        this.scoreCard.leaveRoom(scoreIds);
        let socketFancy = this.socketServiceRedis;
        let socketMatchOdds = this.socketServiceRedisMarket;
        _.map(oldfancy, function (e) {
          socketFancy.leaveRoom(e);
        })
        _.map(oldMatch, function (s) {
          socketMatchOdds.leaveRoom(s);
        })
      });

    // this.selectedUniqIndx = 10;
    // this.getAllSportsData(this.selectedUniqIndx);



    this.socketService
      .changeFlag()
      .subscribe((response) => {
        if(response){
          // this.getAllSportsData('');
          this.getMarketOddsData();
        }
      });

    this.socketServiceClient
      .marketCreate()
      .subscribe((response) => {
        if(response){
          // this.getAllSportsData('');
          this.getMarketOddsData();
        }
      });

    this.socketServiceClient
      .updateSport()
      .subscribe((response) => {
        if(response){
          // this.getAllSportsData('');
          this.getMarketOddsData();
        }
      });

    this.socketServiceClient
      .updateTournament()
      .subscribe((response) => {
        if(response){
          // this.getAllSportsData('');
          this.getMarketOddsData();
        }
      });

    this.socketServiceClient
      .updateMatch()
      .subscribe((response) => {
        if(response){
          // this.getAllSportsData('');
          this.getMarketOddsData();
        }
      });

    this.socketServiceRedisMarket
      .oddsRate()
      .subscribe((response) => {
        if (response) {
          if (response.status == 'OPEN') {
            let marketId = response.marketId.toString().replace('.', '');
            let runtypes = response.runners;
            let runners = 0;
            if (response.numberOfActiveRunners) {
              runners = response.numberOfActiveRunners;
            } else {
              runners = response.runners.length;
            }
            if(runners == 3){
              $("#" + marketId + "_back_2_odds_hiddne").val((runtypes[2].ex.availableToBack[0]) ? runtypes[2].ex.availableToBack[0].price : '-');
              $("#" + marketId + "_lay_2_odds_hiddne").val((runtypes[2].ex.availableToLay[0]) ? runtypes[2].ex.availableToLay[0].price : '-');
              const currOddsRate2 =  $('#' + marketId +'_back_2_odds').text();
              const prevOddsRate2 = $('#' + marketId +'_back_2_odds_hiddne').val();
              const laycurrOddsRate2 =  $('#' + marketId +'_lay_2_odds').text();
              const layprevOddsRate2 = $('#' + marketId +'_lay_2_odds_hiddne').val();
              if (currOddsRate2 === prevOddsRate2) {
                $('#' + marketId +'_back_2_odds').css('background', '');
              } else {
                $('#' + marketId +'_back_2_odds').css('background', '#FFD700');
              }
              if (laycurrOddsRate2 === layprevOddsRate2) {
                $('#' + marketId +'_lay_2_odds').css('background', '');
              } else {
                $('#' + marketId +'_lay_2_odds').css('background', '#FFD700');
              }
            }
            $("#" + marketId + "_back_1_odds_hiddne").val((runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToBack[0]) ? runtypes[0].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_1_odds_hiddne").val((runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToLay[0]) ? runtypes[0].ex.availableToLay[0].price : '-');
            $("#" + marketId + "_back_3_odds_hiddne").val((runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_3_odds_hiddne").val((runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');

            // Start Blinking Rate change Odds
            const currOddsRate =  $('#' + marketId +'_back_1_odds').text();
            const prevOddsRate = $('#' + marketId +'_back_1_odds_hiddne').val();
            const currOddsRate1 =  $('#' + marketId +'_back_3_odds').text();
            const prevOddsRate1 = $('#' + marketId +'_back_3_odds_hiddne').val();


            const laycurrOddsRate =  $('#' + marketId +'_lay_1_odds').text();
            const layprevOddsRate = $('#' + marketId +'_lay_1_odds_hiddne').val();
            const laycurrOddsRate1 =  $('#' + marketId +'_lay_3_odds').text();
            const layprevOddsRate1 = $('#' + marketId +'_lay_3_odds_hiddne').val();

            if (currOddsRate === prevOddsRate) {
              $('#' + marketId +'_back_1_odds').css('background', '');
            } else {
              $('#' + marketId +'_back_1_odds').css('background', '#FFD700');
            }
            if (currOddsRate1 === prevOddsRate1) {
              $('#' + marketId +'_back_3_odds').css('background', '');
            } else {
              $('#' + marketId +'_back_3_odds').css('background', '#FFD700');
            }

            if (laycurrOddsRate === layprevOddsRate) {
              $('#' + marketId +'_lay_1_odds').css('background', '');
            } else {
              $('#' + marketId +'_lay_1_odds').css('background', '#FFD700');
            }
            if (laycurrOddsRate1 === layprevOddsRate1) {
              $('#' + marketId +'_lay_3_odds').css('background', '');
            } else {
              $('#' + marketId +'_lay_3_odds').css('background', '#FFD700');
            }

            $("#" + marketId + "_back_1_odds").html((runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToBack[0]) ? runtypes[0].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_1_odds").html(( runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToLay[0]) ? runtypes[0].ex.availableToLay[0].price : '-');
            if (runners == 3) {
              $("#" + marketId + "_back_3_odds").html(( runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
              $("#" + marketId + "_lay_3_odds").html(( runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');
              $("#" + marketId + "_back_2_odds").html((runtypes[2] && runtypes[2].ex && runtypes[2].ex.availableToBack[0]) ? runtypes[2].ex.availableToBack[0].price : '-');
              $("#" + marketId + "_lay_2_odds").html(( runtypes[2] && runtypes[2].ex && runtypes[2].ex.availableToLay[0]) ? runtypes[2].ex.availableToLay[0].price : '-');
            } else {
              if (runtypes[1] && runtypes[1].ex) {
                $("#" + marketId + "_back_3_odds").html((runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
                $("#" + marketId + "_lay_3_odds").html((runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');
              }
            }
          }
        }
      });
    this.getBannerImage();
  }

  ngOnDestroy() {
    const oldMatch = JSON.parse(sessionStorage.getItem('matchOddsIds'));
    const oldfancy = JSON.parse(sessionStorage.getItem('lastJoinRoom'));
    let scoreIds =  localStorage.getItem('scorecardIds');
    this.scoreCard.leaveRoom(scoreIds);
    let socketFancy = this.socketServiceRedis;
    let socketMatchOdds = this.socketServiceRedisMarket;
    _.map(oldfancy, function (e) {
      socketFancy.leaveRoom(e);
    })
    _.map(oldMatch, function (s) {
      socketMatchOdds.leaveRoom(s);
    })
  }
  getBannerImage(){
    let bannerImage = localStorage.getItem('bannerImages');
    bannerImage = JSON.parse(bannerImage);
    let prefix = env.socketPrefix1();
    if(bannerImage){
      this.bannerImages = _.filter(bannerImage , function (e) {
        let items = _.includes(e.whitelableTypes , prefix);
        if(e.type === 'Banner' && items){
          return e;
        }
      });
      this.casinoImages = _.filter(bannerImage , function (e) {
        let items1 = _.includes(e.whitelableTypes , prefix);
        if(e.type === 'Casino' && items1){
          return e;
        }
      });
      setTimeout(function(){
        $('.carousel-indicators > li').first().addClass('active');
        $('#img0').addClass('active');
      }, 50);
    }
    this.marketService.getBannerImage().subscribe(response => {
      if (response.status) {
        localStorage.setItem('bannerImages', JSON.stringify(response.data));
      } else {
        // this.commonService.popToast('error','Error', 3000 , 'not found market rules.');
      }
    }, error => {
      // this.commonService.popToast('error', 'Error', 3000, 'not found.');
    });
  }

  getInitialRate(){
    this.marketService.getInitialRate().subscribe(response => {
      localStorage.setItem( "dummyRateResponse", JSON.stringify(response.data));
    });
    this.marketService.getInitialRateMarket().subscribe(response => {
      localStorage.setItem( "dummyMarketRateResponse", JSON.stringify(response.data));
    });
  }


  dashboardData(type){
    this.getInitialRate();

    let mymatchOddsId = [];
    let matchOdds = this.socketServiceRedisMarket;
    if(type == 'inplay'){
      this.getMarketOddsItem();
      let items = localStorage.getItem("inplayMatch");
      items = JSON.parse(items)
      let data = _.toArray(items);
      if(data && data.length > 0 && data[0] != null ) {
        data.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            firstLoop.marketDetails.map(secondLoop => {
              if (secondLoop.displayName == "Match Odds") {
                firstLoop.marketIdDec = secondLoop.marketId.toString().replace('.', '');
                mymatchOddsId.push(secondLoop.marketId);
                matchOdds.joinRoom(secondLoop.marketId);
              }
              return secondLoop;
            });
            return firstLoop;
          });
          return marketLoop;
        });
      }
      this.getAllMarkets = data
    }else if(type == 'CRICKET'){
      this.getMarketOddsData();
      let items = localStorage.getItem("cricketData");
      items = JSON.parse(items)
      let data = [];
      data.push(items);
      if(data.length > 0 && data[0] != null ) {
        data.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            firstLoop.marketDetails.map(secondLoop => {
              if (secondLoop.displayName == "Match Odds") {
                firstLoop.marketIdDec = secondLoop.marketId.toString().replace('.', '');;
                mymatchOddsId.push(secondLoop.marketId);
                matchOdds.joinRoom(secondLoop.marketId);
              }
              return secondLoop;
            });
            return firstLoop;
          });
          return marketLoop;
        });
      }
    this.getAllMarkets = data

    }else if(type == 'SOCCER'){
      this.getMarketOddsData();
      let items = localStorage.getItem("tennisData");
      items = JSON.parse(items)
      let data = [];
      data.push(items);
      if(data.length > 0 && data[0] != null ) {
        data.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            firstLoop.marketDetails.map(secondLoop => {
              if (secondLoop.displayName == "Match Odds") {
                firstLoop.marketIdDec = secondLoop.marketId.toString().replace('.', '');;
                mymatchOddsId.push(secondLoop.marketId);
                matchOdds.joinRoom(secondLoop.marketId);
              }
              return secondLoop;
            });
            return firstLoop;
          });
          return marketLoop;
        });
      }
      this.getAllMarkets = data

    }else if(type == 'Tennis'){
      this.getMarketOddsData();
      let items = localStorage.getItem("soccerData");
      items = JSON.parse(items)
      let data = [];
      data.push(items);
      if(data.length > 0 && data[0] != null ) {
        data.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            firstLoop.marketDetails.map(secondLoop => {
              if (secondLoop.displayName == "Match Odds") {
                firstLoop.marketIdDec = secondLoop.marketId.toString().replace('.', '');;
                mymatchOddsId.push(secondLoop.marketId);
                matchOdds.joinRoom(secondLoop.marketId);
              }
              return secondLoop;
            });
            return firstLoop;
          });
          return marketLoop;
        });
      }
      this.getAllMarkets = data

    }else{

    }
    setTimeout(() => {
      if(this.dummyRateResponse){
        this.appendInitialData(JSON.parse(this.dummyRateResponse));
      }
    }, 1);
    let reply = localStorage.getItem("dashboardData");
    if(reply){
      reply = JSON.parse(reply);
    }

  }
  getMarketOddsData() {
    this.marketService.getAllMarketForDashboard(this.marketFilter, this.userId).subscribe(response => {
      localStorage.setItem('dashboardData', JSON.stringify(response.data));
      if(response.data){
        _.map(response.data, function (e) {
          if(e._id == 4){
            localStorage.setItem('cricketData', JSON.stringify(e));
          } else if(e._id == 1){
            localStorage.setItem('tennisData', JSON.stringify(e));
          }else if(e._id == 2){
            localStorage.setItem('soccerData', JSON.stringify(e));
          }else{
            localStorage.setItem('otherSport', JSON.stringify(e));
          }
        });
      }
    });
    setTimeout(() => {
      if(this.dummyRateResponse){
        this.appendInitialData(JSON.parse(this.dummyRateResponse));
      }
    }, 10);
  }

  getMarketOddsItem() {
    let reply = localStorage.getItem("dashboardData")
    if(reply){
      reply = JSON.parse(reply);
      reply =    _.filter(reply, function (e) {
        e.doc = _.filter(e.doc, function (i) {
          // i.data = _.filter(i.data, function (p) {
          if(i.isPlay == true){
            return i;
          }
          // })
          // if(i.data.length > 0){
          //   return i;
          // }

        })
        if(e.doc.length > 0) {
          return e;
        }
      })
      let gfg = _.sortBy(reply, ['doc[0].sportUrl.displayOrder']);
      localStorage.setItem('inplayMatch',JSON.stringify(gfg));

    }
  }

  getMarketOdds(data, index) {
    let mymatchOddsId = [];
    let matchOdds = this.socketServiceRedisMarket;
    this.spinner.hide();
    let datas = localStorage.getItem(data.toString());
    if(datas !== null) {
      datas = this.utilityService.gsk(datas);
      this.getAllMarkets = JSON.parse(datas).data;
      this.getAllMarkets.map(marketLoop => {
        marketLoop.doc = marketLoop.doc.map(firstLoop => {
          // firstLoop.data.map(secondLoop => {
          if (firstLoop.marketId && firstLoop.marketId.length > 0) {
            firstLoop.marketIdDec = firstLoop.marketId[0].toString().replace('.', '');
            mymatchOddsId.push(firstLoop.marketId[0]);
            matchOdds.joinRoom(firstLoop.marketId[0]);
          }
          // });
          return firstLoop;
        });
        sessionStorage.setItem('matchOddsIds' , JSON.stringify(mymatchOddsId));
        return marketLoop;
      });
      $('#inplay').removeClass('active');
      this.contentHide = false;
      this.selectedUniqIndx = index;
      this.marketFilter.data = data;
      let userId = this.utilityService.returnLocalStorageData('userId');
      this.marketService.getAllMarketForDashboard(this.marketFilter, userId).subscribe(response => {
        localStorage.setItem(data, response.auth);
        this.spinner.hide();
      });
    }else{
      $('#inplay').removeClass('active');
      this.contentHide = false;
      this.selectedUniqIndx = index;
      this.marketFilter.data = data;
      let userId = this.utilityService.returnLocalStorageData('userId');
      this.marketService.getAllMarketForDashboard(this.marketFilter, userId).subscribe(response => {
        localStorage.setItem(data, response.auth);
        response = this.utilityService.gsk(response.auth);
        response = JSON.parse(response);
        this.getAllMarkets = response.data;
        this.spinner.hide();
        // this.socketServiceRedisMarket.connect();
        this.getAllMarkets.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            // firstLoop.data.map(secondLoop => {
            if (firstLoop.marketId && firstLoop.marketId.length > 0) {
              firstLoop.marketIdDec = firstLoop.marketId[0].toString().replace('.', '');
              mymatchOddsId.push(firstLoop.marketId[0]);
              matchOdds.joinRoom(firstLoop.marketId[0]);
              sessionStorage.setItem('matchOddsIds' , JSON.stringify(mymatchOddsId));
            }
            // });
            return firstLoop;
          });
          return marketLoop;
        });
      });
    }
  }



  getAllSportsData(ite) {
    this.marketService.getAllSport().subscribe(response => {
      response = this.utilityService.gsk(response.auth);
      response = JSON.parse(response);
      if(response) {
        this.getAllSports = response.data;
        if (this.getAllSports.length > 0 && ite !== 10) {
          let index = this.selectedUniqIndx ? this.selectedUniqIndx : 0;
          // this.getMarketOdds(this.getAllSports[index].id, index);
        } else {
          // this.getAllMatchData();
        }
      }
    });
  }

  getAllMatchData() {
    let mymatchOddsId = [];
    let matchOdds = this.socketServiceRedisMarket;
    this.spinner.hide();
    let datas = localStorage.getItem('inplay');
    if(datas !== null){
      datas = this.utilityService.gsk(datas);
      this.getAllMarkets = JSON.parse(datas).data;
      this.getAllMarkets.map(marketLoop => {
        marketLoop.doc = marketLoop.doc.map(firstLoop => {
          // firstLoop.data.map(secondLoop => {
          if (firstLoop.marketId && firstLoop.marketId.length > 0) {
            firstLoop.marketIdDec = firstLoop.marketId[0].toString().replace('.', '');
            matchOdds.joinRoom(firstLoop.marketId[0]);
            mymatchOddsId.push(firstLoop.marketId[0]);
            sessionStorage.setItem('matchOddsIds' , JSON.stringify(mymatchOddsId));
          }
          // });
          return firstLoop;
        });
        return marketLoop;
      });
      this.marketService.getAllMatchInMarket().subscribe(response => {
        localStorage.setItem('inplay', response.auth);
        this.spinner.hide();
      });
    }else{
      // this.socketServiceRedisMarket.disconnect();
      // this.contentHide = true;
      $('#mytab a').removeClass('active');
      $('#inplay').addClass('active');
      this.marketService.getAllMatchInMarket().subscribe(response => {
        localStorage.setItem('inplay', response.auth);
        response = this.utilityService.gsk(response.auth);
        response = JSON.parse(response);
        this.getAllMarkets = response.data;
        this.spinner.hide();
        // this.socketServiceRedisMarket.connect();
        this.getAllMarkets.map(marketLoop => {
          marketLoop.doc = marketLoop.doc.map(firstLoop => {
            // firstLoop.data.map(secondLoop => {
            if (firstLoop.marketId && firstLoop.marketId.length > 0) {
              firstLoop.marketIdDec = firstLoop.marketId[0].toString().replace('.', '');
              matchOdds.joinRoom(firstLoop.marketId[0]);
              mymatchOddsId.push(firstLoop.marketId[0]);
              sessionStorage.setItem('matchOddsIds' , JSON.stringify(mymatchOddsId));
            }
            // });
            return firstLoop;
          });
          return marketLoop;
        });
      });
    }
  }

  commingSoon(){
    this.showToster('Error','error', "Comming Soon");
  }


  showToster(title,type,message){
    this.Toast.title = title;
    this.Toast.type = type;
    this.Toast.body = message;
    this.commonService.popToast('success','Success', 3000 , message);
  }

  closeOtpModal(){
    this.otpModal.hide();
  }


  appendInitialData(dummyRateResponse){
    // dummyRateResponse = JSON.parse(dummyRateResponse);
    if(dummyRateResponse){
      _.map(dummyRateResponse, function (upendData) {
        upendData = JSON.parse(upendData)
        let obj = {
          "marketId": upendData.marketId.toString().replace('.', ''),
          "status": "OPEN",
          "numberOfRunners": 2,
        }

        obj['runners'] = upendData.numberOfRunners;
        let marketId = obj.marketId;
        let runtypes = upendData.runners;
        let runners = 0;
        if (obj['numberOfActiveRunners']) {
          runners = obj['numberOfActiveRunners'];
        } else {
          runners = upendData.numberOfRunners;
        }
        if(runners > 0){
          if(runners == 3){
            $("#" + marketId + "_back_2_odds_hiddne").val((runtypes[2].ex.availableToBack[0]) ? runtypes[2].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_2_odds_hiddne").val((runtypes[2].ex.availableToLay[0]) ? runtypes[2].ex.availableToLay[0].price : '-');
            const currOddsRate2 =  $('#' + marketId +'_back_2_odds').text();
            const prevOddsRate2 = $('#' + marketId +'_back_2_odds_hiddne').val();
            const laycurrOddsRate2 =  $('#' + marketId +'_lay_2_odds').text();
            const layprevOddsRate2 = $('#' + marketId +'_lay_2_odds_hiddne').val();
            if (currOddsRate2 === prevOddsRate2) {
              $('#' + marketId +'_back_2_odds').removeClass('yellow');
            } else {
              $('#' + marketId +'_back_2_odds').addClass('yellow');
            }
            if (laycurrOddsRate2 === layprevOddsRate2) {
              $('#' + marketId +'_lay_2_odds').removeClass('yellow');
            } else {
              $('#' + marketId +'_lay_2_odds').addClass('yellow');
            }
          }
          $("#" + marketId + "_back_1_odds_hiddne").val("3.05");
          $("#" + marketId + "_lay_1_odds_hiddne").val((runtypes && runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToLay[0]) ? runtypes[0].ex.availableToLay[0].price : '-');
          $("#" + marketId + "_back_3_odds_hiddne").val((runtypes && runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
          $("#" + marketId + "_lay_3_odds_hiddne").val((runtypes && runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');

          // Start Blinking Rate change Odds
          const currOddsRate =  $('#' + marketId +'_back_1_odds').text();
          const prevOddsRate = $('#' + marketId +'_back_1_odds_hiddne').val();
          const currOddsRate1 =  $('#' + marketId +'_back_3_odds').text();
          const prevOddsRate1 = $('#' + marketId +'_back_3_odds_hiddne').val();


          const laycurrOddsRate =  $('#' + marketId +'_lay_1_odds').text();
          const layprevOddsRate = $('#' + marketId +'_lay_1_odds_hiddne').val();
          const laycurrOddsRate1 =  $('#' + marketId +'_lay_3_odds').text();
          const layprevOddsRate1 = $('#' + marketId +'_lay_3_odds_hiddne').val();

          // console.log(prevOddsRate)
          if (currOddsRate === prevOddsRate) {
            $('#' + marketId +'_back_1_odds').removeClass('yellow');
          } else {
            $('#' + marketId +'_back_1_odds').addClass('yellow');
          }
          if (currOddsRate1 === prevOddsRate1) {
            $('#' + marketId +'_back_3_odds').removeClass('yellow');
          } else {
            $('#' + marketId +'_back_3_odds').addClass('yellow');
          }

          if (laycurrOddsRate === layprevOddsRate) {
            $('#' + marketId +'_lay_1_odds').removeClass('yellow');
          } else {
            $('#' + marketId +'_lay_1_odds').addClass('yellow');
          }
          if (laycurrOddsRate1 === layprevOddsRate1) {
            $('#' + marketId +'_lay_3_odds').removeClass('yellow');
          } else {
            $('#' + marketId +'_lay_3_odds').addClass('yellow');
          }

          $("#" + marketId + "_back_1_odds").html((runtypes && runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToBack[0]) ? runtypes[0].ex.availableToBack[0].price : '-');
          $("#" + marketId + "_lay_1_odds").html(( runtypes && runtypes[0] && runtypes[0].ex && runtypes[0].ex.availableToLay[0]) ? runtypes[0].ex.availableToLay[0].price : '-');
          if (runners == 3) {
            $("#" + marketId + "_back_3_odds").html(( runtypes && runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_3_odds").html((runtypes &&  runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');
            $("#" + marketId + "_back_2_odds").html((runtypes && runtypes[2] && runtypes[2].ex && runtypes[2].ex.availableToBack[0]) ? runtypes[2].ex.availableToBack[0].price : '-');
            $("#" + marketId + "_lay_2_odds").html(( runtypes && runtypes[2] && runtypes[2].ex && runtypes[2].ex.availableToLay[0]) ? runtypes[2].ex.availableToLay[0].price : '-');
          } else {
            if (runtypes[1] && runtypes[1].ex) {
              $("#" + marketId + "_back_3_odds").html((runtypes && runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToBack[0]) ? runtypes[1].ex.availableToBack[0].price : '-');
              $("#" + marketId + "_lay_3_odds").html((runtypes && runtypes[1] && runtypes[1].ex && runtypes[1].ex.availableToLay[0]) ? runtypes[1].ex.availableToLay[0].price : '-');
            }
          }
        }
      });
    }
  }

  getUserCurrency(){
    const data = {
      userId : this.userId
    };
    this.currencyService.getUserCurrency(data).subscribe(response => {
      response = this.utilityService.gsk(response.auth);
      response = JSON.parse(response);
      if(response.status == true) {
        localStorage.setItem('currencyAll', JSON.stringify(response.data[0]))
        // this.currencyAll = response.data[0];
      }
    });
  }
}
