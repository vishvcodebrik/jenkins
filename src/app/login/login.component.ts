'use strict';
import {SocketService} from "../globals/socketService";

declare var require: any;
import { Component, OnInit,ViewChild } from '@angular/core';
import {Route, Router} from '@angular/router';
import * as env from '../globals/env';
import { CommonService } from '../services/common.service';
import { UserService } from '../services/user.service';
import {SocketServiceClient} from "../globals/socketServiceClient";
var aes256 = require('aes256');
import { UtilityService } from '../globals/utilityService';
import {environment} from '../../environments/environment';
import {ModalDirective} from 'ngx-bootstrap';
// import { BinaryService } from '../services/binary.service';
import * as $ from 'jquery';
import {MarketService} from '../services/market.service';
import {NgxSpinnerService} from "ngx-spinner";
import {SocketServiceRedis} from '../globals/socketServiceRedis';
import { NgForm } from '@angular/forms';
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/timer";
import "rxjs/add/operator/finally";
import "rxjs/add/operator/takeUntil";
import "rxjs/add/operator/map";
import {StudioService} from '../services/studio.service';

declare let _: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  currentApplicationVersion = environment.appVersion;
  @ViewChild("loginForm", { static: false }) loginForm;
  @ViewChild('loginModal', { static: false }) loginModal: ModalDirective;
  @ViewChild('passwordChange', { static: false }) passwordChange: ModalDirective;
  @ViewChild("registerForm", { static: false }) registerForm;
  @ViewChild('registerModal', { static: false }) registerModal: ModalDirective;
  @ViewChild('otpModal', { static: false }) otpModal: ModalDirective;
  slideConfig = {"slidesToShow": 5, "slidesToScroll": 1, "autoplay": true,  "vertical": false , arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };
  slideConfig1 = {"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true,  "vertical": false , arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };
  logo: any = env.PROJECT_LOGO_URL;
  oldPassword: any;
  newPassword: any;
  bannerImages: any;
  casinoImages: any;
  confNewPassword: any;
  age18year = true;
  loginObject = {
    userName: null,
    password: null,
    deviceId : "",
    fcmToken : "",
    channel : "WEB",
    userType : "CLIENT"
  };
  registerObject = {
    userName: null,
    fullName:null,
    password: null,
    channel : "WEB",
    userType : "CLIENT",
    dialCode : "91",
    countryName : "India (भारत)",
    mobileNo : '',
    emailId : ''
  };
  showPassword = false;
  showPassword1 = false;
  showPassword2 = false;
  Toast = {
    type: '',
    title: '',
    timeout: 0,
    body: ''
  };
  endSubmit = false;
  stockUsernamePrefix = env.stockUsernamePrefix();
  refralCode = false;
  countryCode = {
    dialCode: "91",
    iso2: "in",
    name: "India (भारत)"
  }
  brandImages = ['../../assets/images/evolution-gaming.png','../../assets/images/ezugi.png','../../assets/images/realtime-gaming.png','../../assets/images/super-spades.png']


  marketFilter = {
    page: 1,
    limit: 10,
    search: null,
    userId : ""
  };
  userId = "";
  otpuserId;
  loginPassword;
  loginUsername;
  countdown: number;
  stopCount  = false;
  gameList;
  groupList;
  groupLists;
  product = ['Evolution Gaming', 'eBET' ,'Super Spade Games', 'Pragmatic Play Live', 'Asia Gaming', 'Evoplay Entertainment', 'Betsoft', 'Ezugi'];
  gameLists;
  constructor( private router: Router,
               private commonService: CommonService,
               private socketService: SocketService,
               private socketServiceClient: SocketServiceClient,
               private socketServiceRedis: SocketServiceRedis,
               private studioService: StudioService,
               private utilityService: UtilityService ,
               private userService:UserService,
               private marketService:MarketService,
               // private binaryService : BinaryService,
               private spinner : NgxSpinnerService
  ) { }

  ngOnInit() {
    $('.loading').hide();
    localStorage.clear();
    $('#mode').attr("href", 'assets/scss/custome-dark.css');
    this.socketService.disconnect();
    this.socketServiceClient.disconnect();
    this.socketServiceRedis.disconnect();
    let data = JSON.parse(this.utilityService.returnLocalStorageData('userData'));
    if (!this.utilityService.returnLocalStorageData('userId')) {
    } else {
      if(data && data.userType === 'CLIENT'){
        localStorage.clear();
        // this.router.navigate(['/dashboard']);
      }
    }
    this.getBannerImage();
    this.getCasinoList();
  }
  ngAfterViewInit(){
    this.getMarketOdds();
    this.getInitialRate();


  }

  getCasinoList(){
    let data = {
      product : this.product
    }
    this.studioService.getGameList(data).subscribe(response => {
      if(response.status == true){
        this.gameLists = response.data;
        this.gameList = response.data;
        localStorage.setItem('casinoList', JSON.stringify(response.data))
        // this.instructionModal.show();
        this.groupList = _.chain(this.gameLists)
        // Group the elements of Array based on `color` property
          .groupBy("product")
          // `key` is group's name (color), `value` is the array of objects
          .map((value, key) => ({ name: key, category: value }))
          .value();
      }else{
        this.gameList = [];
      }
    });
  }

  getBannerImage(){
    let prefix = env.socketPrefix1();
    this.marketService.getBannerImage().subscribe(response => {
      if (response.status) {
        localStorage.setItem('bannerImages', JSON.stringify(response.data));
        this.bannerImages = _.filter(response.data , function (e) {
          let items = _.includes(e.whitelableTypes , prefix);
          if(e.type === 'Banner' && items){
            return e;
          }
        });
        this.casinoImages = _.filter(response.data , function (e) {
          let items1 = _.includes(e.whitelableTypes , prefix);
          if(e.type === 'Casino' && items1){
            return e;
          }
        });
        setTimeout(function(){
          $('.carousel-indicators > li').first().addClass('active');
          $('#img0').addClass('active');
        }, 200);

      } else {
        // this.commonService.popToast('error','Error', 3000 , 'not found market rules.');
      }
    }, error => {
      // this.commonService.popToast('error', 'Error', 3000, 'not found.');
    });
  }

  getMarketOdds() {
    this.marketService.getAllMarketForDashboard(this.marketFilter, this.userId).subscribe(response => {
      localStorage.setItem('dashboardData', JSON.stringify(response.data));
    });

    this.marketService.getAllMarketForSideCreative(this.marketFilter, this.userId).subscribe(response => {
      localStorage.setItem('sidebarData', JSON.stringify(response.data));
    });

  }

  getInitialRate(){
    this.marketService.getInitialRate().subscribe(response => {
      localStorage.setItem( "dummyRateResponse", JSON.stringify(response.data));
    });
    // this.marketService.getInitialRateMarket().subscribe(response => {
    //   localStorage.setItem( "dummyMarketRateResponse", JSON.stringify(response.data));
    // });

  }
  /*
    Developer: Ravi
    Date: 09-jan-2020
    title: Login method from backend
    Use: This function is use login from backend
  */

  login() {
    this.spinner.show();
    if(this.endSubmit) {
      return;
    }
    this.endSubmit = true;
    let key = env.constantKey();
    this.userService.loginApi(this.loginObject).subscribe(resposne =>{
      this.endSubmit = false;
      resposne = this.utilityService.gsk(resposne.auth);
      resposne = JSON.parse(resposne);
      if(resposne.status === true){
        sessionStorage.setItem('balance', 'true');
        sessionStorage.setItem('available', 'true');
        sessionStorage.setItem('exposure', 'true');
        sessionStorage.setItem('pl', 'true');
        //this.showToster('Success','success', resposne.message);

        var descryptJson = aes256.decrypt(key, resposne.data);
        let resposne1 = JSON.parse(descryptJson);
        let userData = JSON.stringify(resposne1);
        let userId = resposne1.user_id;
        let ip = resposne1.ipAddress;
        let dateTime = resposne1.dateTime;
        var userData1 = aes256.encrypt(key, userData);
        var userId1 = aes256.encrypt(key, userId);
        var ip1 = aes256.encrypt(key, ip);
        var dateTime1 = aes256.encrypt(key, dateTime);
        localStorage.setItem('userId', userId1);
        sessionStorage.setItem('userId', userId1);
        localStorage.setItem('token', resposne.token);
        sessionStorage.setItem('token', resposne.token);

        if(resposne1.changePassword === true){
          localStorage.setItem('userData', userData1);
          localStorage.setItem('ip', ip1);
          localStorage.setItem('dateTime', dateTime1);

          if(resposne1.allowStock != true){
            this.loginModal.hide();
            this.spinner.hide();
            sessionStorage.setItem('theme', resposne1.themeName);
            localStorage.setItem('theme', resposne1.themeName);
            $('#mode').attr("href", resposne1.themeName);
            this.router.navigate(['/dashboard']);
          }

        }else{
          this.spinner.hide();
          this.loginModal.hide();
          this.passwordChange.show();
        }
        this.spinner.hide();
        if(resposne1.changePassword === true){
          this.router.navigate(['/dashboard']);
        }
      }else{
        this.spinner.hide();
        // this.loginObject.password = '';
        this.showToster('Error','error', resposne.message);
      }
    }, error =>{
      this.spinner.hide();
      let input = document.getElementById('userName');
      input.focus();
      // this.loginObject.password = '';
      this.loginForm.resetForm();
      this.showToster('Error','error', error.error.message);
    });
  }

  register(form: NgForm){
    this.spinner.show();
    if(this.endSubmit) {
      return;
    }
    this.endSubmit = true;
    this.registerObject.dialCode = this.countryCode.dialCode;
    this.registerObject.countryName = this.countryCode.name;
    this.userService.registerApi(this.registerObject).subscribe(resposne =>{
      this.endSubmit = false;
      this.spinner.hide();
      resposne = this.utilityService.gsk(resposne.auth);
      resposne = JSON.parse(resposne);
      if(resposne.status == true){
        console.log("resposne++++++++",resposne);
        this.otpuserId = resposne.data._id;
        this.closeRegister();
        // this.loginObject = {
        //   channel: "WEB",
        //   deviceId: "",
        //   fcmToken: "",
        //   password: this.registerObject.password,
        //   userName:  this.registerObject.userName,
        //   userType: "CLIENT"
        // }
        this.loginPassword = this.registerObject.password;
        this.loginUsername = this.registerObject.userName;
        form.resetForm();
        this.otpModal.show();
        this.startCountdownTimer();
        this.showToster('Success','success', resposne.message);
      }else{
        this.showToster('Error','error', resposne.message);
      }
    }, error =>{
      this.showToster('Error','error', error.error.message);
    })
  }


  /*
      Developer: Ravi
      Date: 05-mar-2020
      title: Toster message
      Use: This function is use to throgh toster message
    */

  showToster(title,type,message){
    this.Toast.title = title;
    this.Toast.type = type;
    this.Toast.body = message;
    this.commonService.popToast(type, title, 1500, message)
  }

  showPass(){
    this.showPassword = (this.showPassword === true) ?  false : true;
  }
  showPass1(){
    this.showPassword1 = (this.showPassword1 === true) ?  false : true;
  }
  showPass2(){
    this.showPassword2 = (this.showPassword2 === true) ?  false : true;
  }


  closeLogin(){
    this.loginModal.hide();
  }
  closeRegister(){
    this.registerModal.hide();
  }
  closeModal(){
    this.passwordChange.hide();
  }
  closeOtp(){
    this.otpModal.hide();
  }
  loginModalData(){
    this.loginModal.show();
  }
  registerModalData(){
    this.registerModal.show();
  }
  passChange() {
    let stockUserId = this.utilityService.returnLocalStorageData('stockUserId')
    if(stockUserId ==  false){
      let data = {
        oldPass: this.utilityService.returnEncrypt(this.oldPassword),
        newPass: this.utilityService.returnEncrypt(this.newPassword),
        cinfNewPass: this.utilityService.returnEncrypt(this.confNewPassword),
        userId: this.utilityService.returnLocalStorageData('userId'),
        stockUserId : stockUserId
      };
      this.userService.passwordChange(data).subscribe(response => {
        this.closeModal();
        if (response.status === true) {
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigate(['/login']);
        } else {
          this.commonService.popToast('error', 'Error', 1500, 'password does not match.');
          this.router.navigate(['/login']);
        }
      }, error => {
        this.closeModal();

        this.commonService.popToast('error', 'Error', 1500, 'password does not match.');
        this.router.navigate(['/login']);
        console.error("error in logout");
      });
    }else{
      this.userService.getNewToken().subscribe(responseToken => {
        if(responseToken.status == true){
          let refreshToken = responseToken.data.refresh_token;
          this.utilityService.storeRefreshToken(refreshToken);
          let data = {
            oldPass: this.utilityService.returnEncrypt(this.oldPassword),
            newPass: this.utilityService.returnEncrypt(this.newPassword),
            cinfNewPass: this.utilityService.returnEncrypt(this.confNewPassword),
            userId: this.utilityService.returnLocalStorageData('userId'),
            stockUserId : stockUserId,
            accessToken : responseToken.data.access_token
          };
          this.userService.passwordChange(data).subscribe(response => {
            this.closeModal();

            if (response.status === true) {
              localStorage.clear();
              sessionStorage.clear();
              this.router.navigate(['/login']);
            } else {
              this.commonService.popToast('error', 'Error', 1500, 'password does not match.');
              this.router.navigate(['/login']);
            }
          }, error => {
            this.closeModal();

            this.commonService.popToast('error', 'Error', 1500, 'password does not match.');
            this.router.navigate(['/login']);
            console.error("error in logout");
          });
        }
      });
    }
  }
  checkValue(event){
    this.age18year = event.target.checked;
  }

  getNumber(event){
    console.log(event);
  }

  telInputObject(event){
    console.log(event);
  }

  onCountryChange(event){
    let obj = {
      dialCode: event.dialCode,
      iso2: event.iso2,
      name: event.name
    }
    this.countryCode = obj;
  }

  verifyOtp(){
    this.spinner.show();
    if(this.endSubmit) {
      return;
    }
    this.endSubmit = true;
    let otp =$("#otp").val();
    let otpObj = {
      userId : this.otpuserId,
      otp : otp
    }
    this.userService.verifyOtp(otpObj).subscribe(resposne =>{
      this.endSubmit = false;
      this.spinner.hide();
      resposne = this.utilityService.gsk(resposne.auth);
      resposne = JSON.parse(resposne);
      if(resposne.status == true){
        this.loginObject = {
          channel: "WEB",
          deviceId: "",
          fcmToken: "",
          password: this.loginPassword,
          userName:  this.loginUsername,
          userType: "CLIENT"
        }
        this.otpModal.hide();
        this.login();
      }else{
        this.showToster('Error','error', resposne.message);
      }
    })
  }


  startCountdownTimer() {
    const interval = 1000;
    const duration = 120 * 1000;
    const stream$ = Observable.timer(0, interval)
      .finally(() => {
        this.stopCount = true;
      })
      .takeUntil(Observable.timer(duration + interval))
      .map(value => (duration - value * interval)/1000);
    stream$.subscribe(value => this.countdown = value);
  }

  resendOtp(){
    let otpObj = {
      userId : this.otpuserId
    }
    this.userService.resendOtp(otpObj).subscribe(resposne =>{
      resposne = this.utilityService.gsk(resposne.auth);
      resposne = JSON.parse(resposne);
      if(resposne.status == true){
        this.showToster('Success','success', resposne.message);
        this.stopCount = false;
        this.startCountdownTimer();
      }else{
        this.showToster('Error','error', resposne.message);
      }
    });
  }
}


